package com.genisys.botkit.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.TestData;
import com.genisys.botkit.adapters.BotListAdapter;
import com.genisys.botkit.api.pojo.AllBots;
import com.genisys.botkit.api.pojo.BotDetail;
import com.genisys.botkit.api.pojo.OtpVerify;
import com.genisys.botkit.network.ApiServiceInterface;
import com.genisys.botkit.network.AppClient;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.ProgressBuilder;
import com.genisys.botkit.util.Toaster;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BotListFragment extends Fragment implements View.OnClickListener {

    private ListView botListView;
    private TextView emptyView;
    private BotListAdapter adapter;
    private ArrayList<BotDetail> botDetails;
    private ArrayList<String> selectedBotIds;
    private ProgressBuilder progressBuilder;
    private String TAG = BotListFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bot_list, null);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initializeToolBarRightView();
        callBotListApi();

    }

    private void initView(View view) {
        selectedBotIds = new ArrayList<String>();
        botDetails = new ArrayList<BotDetail>();
        progressBuilder = new ProgressBuilder(getActivity());
        botListView = (ListView) view.findViewById(R.id.lvBotList);
        botListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (botDetails.get(position).isSelected) {
                    botDetails.get(position).isSelected = false;
                } else if (!botDetails.get(position).isSelected) {
                    botDetails.get(position).isSelected = true;
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void callRegisterBotApi(JsonObject object) {
        progressBuilder.showProgressDialog();
        ApiServiceInterface client = AppClient.getClient().create(ApiServiceInterface.class);
//        JsonObject object = new JsonObject();
//        object.addProperty("phone", BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.PHONE_NUMBER,""));
//        object.addProperty("bot_registered", botDetails.get(position).id);
        client.postRegisterBot(object).enqueue(new Callback<OtpVerify>() {

            @Override
            public void onResponse(Call<OtpVerify> call, Response<OtpVerify> response) {
                progressBuilder.dismissProgressDialog();
                if (response.isSuccessful())
                    checkBotRegisterResponseBody(response.body());
            }

            @Override
            public void onFailure(Call<OtpVerify> call, Throwable t) {
                progressBuilder.dismissProgressDialog();
                Log.e("Throwable", "Throwable  : " + t.getMessage());
            }
        });
    }

    private void callBotListApi() {

        if (!CommonFunctions.isOnline(getActivity())) {
            Toaster.showShortToast(getActivity(), getString(R.string.internet_connection_not_available));
            return;
        }

//        Gson gson = new Gson();
//        AllBots chatMsg = gson.fromJson(TestData.botListResp, AllBots.class);
//        checkResponseBody(chatMsg);
        progressBuilder.showProgressDialog();
        ApiServiceInterface client = AppClient.getClient().create(ApiServiceInterface.class);

        client.getAllBots().enqueue(new Callback<AllBots>() {
            @Override
            public void onResponse(Call<AllBots> call, Response<AllBots> response) {
                progressBuilder.dismissProgressDialog();
                if (response.isSuccessful())
                    checkResponseBody(response.body());
            }

            @Override
            public void onFailure(Call<AllBots> call, Throwable t) {
                progressBuilder.dismissProgressDialog();
                Log.d("Throwable", "Throwable  : " + t.getMessage());
            }
        });
    }

    private void checkResponseBody(AllBots body) {

        if (null != body.botlist && body.botlist.size() > 0) {
            botDetails = new ArrayList<BotDetail>();
            ArrayList<String> selectedBotList = new ArrayList<String>();
            if (null != BotKitApp.getSavedBotList())
                selectedBotList = BotKitApp.getSavedBotList();

            Log.i(TAG, "SelectedBotList Count " + selectedBotList.size());

            for (int i = 0; i < body.botlist.size(); i++) {
                BotDetail bot = new BotDetail();
                bot.name = body.botlist.get(i).name;
                bot.id = body.botlist.get(i).id;
                bot.photo_url = body.botlist.get(i).photo_url;

                if (null != body.botlist.get(i).style) {
                    BotKitApp.saveBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME + bot.id, body.botlist.get(i).style);
                }

                if (null != selectedBotList && selectedBotList.contains(bot.id))
                    bot.isSelected = true;
                else
                    bot.isSelected = false;

                //if(bot.ispublic.equalsIgnoreCase("true"))

                botDetails.add(bot);

            }

            //BotKitApp.getBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME+"1");
            adapter = new BotListAdapter(getActivity(), botDetails, "reg");
            botListView.setAdapter(adapter);
        }
    }

    private void checkBotRegisterResponseBody(OtpVerify body) {
        switch (Integer.parseInt(body.getStatus())) {
            case 200:
                BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.IS_BOT_REGISTER, "Yes").commit();
                callSelectedBotFragment();
                break;
            case 400:
                if (null != body.message)
                    Toaster.showShortToast(getActivity(), body.message);
                else
                    Toaster.showShortToast(getActivity(), getString(R.string.serverbusy));

                break;
            default:
                Toaster.showShortToast(getActivity(), getString(R.string.serverbusy));
        }
    }

    private void callSelectedBotFragment() {
        saveSelectedBotIds();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, new MyBotsFragment());
        fragmentTransaction.commit();
    }

    private void saveSelectedBotIds() {
        BotKitApp.saveBotList(selectedBotIds);
    }


    private void initializeToolBarRightView() {
        TextView tvTitle = (TextView) (getActivity()).findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.botList));
        ImageView imageAdd = (ImageView) (getActivity()).findViewById(R.id.imageAdd);
        imageAdd.setVisibility(View.GONE);

        TextView tvSave;
        tvSave = (TextView) (getActivity()).findViewById(R.id.tvDone);
        tvSave.setText(getString(R.string.save));
        tvSave.setVisibility(View.VISIBLE);
        tvSave.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        StringBuffer responseText = new StringBuffer();

        ArrayList<BotDetail> botList = adapter.mList;
        if (!selectedBotIds.isEmpty())
            selectedBotIds.clear();

        for (int i = 0; i < botList.size(); i++) {
            BotDetail bot = botList.get(i);

            if (bot.isSelected) {
                selectedBotIds.add(bot.id);
                if (responseText.length() != 0)
                    responseText.append(",");
                responseText.append(bot.id);
            }
        }


        JsonObject object = new JsonObject();
        object.addProperty("phone", BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.PHONE_NUMBER, ""));
        object.addProperty("bot_registered", responseText.toString());
        callRegisterBotApi(object);

    }
}
