package com.genisys.botkit.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.activities.ChatMessageActivity;
import com.genisys.botkit.activities.DynamicForm;
import com.genisys.botkit.activities.HomeActivity;
import com.genisys.botkit.activities.SignUpActivity;
import com.genisys.botkit.adapters.BotListAdapter;
import com.genisys.botkit.api.pojo.BotDetail;
import com.genisys.botkit.api.pojo.UserDetails;
import com.genisys.botkit.api.pojo.UserInfo;
import com.genisys.botkit.network.ApiServiceInterface;
import com.genisys.botkit.network.AppClient;
import com.genisys.botkit.services.GetBotListIntentService;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.ProgressBuilder;
import com.genisys.botkit.util.Toaster;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyBotsFragment extends Fragment implements View.OnClickListener {
    private ListView botListView;
    private TextView emptyView;
    private RelativeLayout fragment_selected_bot;
    private BotListAdapter adapter;
    private ArrayList<BotDetail> botDetails;
    private ProgressBuilder progressBuilder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_bots, null);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initializeToolBarRightView();
    }


    private void initView(View view) {
        progressBuilder = new ProgressBuilder(getActivity());
        botListView = (ListView) view.findViewById(R.id.lvMyBots);
        fragment_selected_bot = (RelativeLayout) view.findViewById(R.id.fragment_selected_bot);
        //fragment_selected_bot.setBackground(CommonFunctions.bgGradientDrawable("#3e7492","#a6c0cd","#255779"));

        emptyView = (TextView) view.findViewById(R.id.empty);
        // ((HomeActivity)getActivity()).setMenuIcon();

        botListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toaster.showShortToast(getActivity(),botDetails.get(position).name+"\n"+botDetails.get(position).id);

                Intent chatIntent = new Intent(getActivity(), ChatMessageActivity.class);
                chatIntent.putExtra(AppConstants.EXTRA_CHAT_SCREEN_TITLE, botDetails.get(position).name);
                chatIntent.putExtra(AppConstants.EXTRA_CHAT_BOT_ID, botDetails.get(position).id);
                startActivity(chatIntent);

            }
        });

        Intent botListService = new Intent(getActivity(), GetBotListIntentService.class);
        getActivity().startService(botListService);
        callUserInfoAPI();
    }

    private void initializeToolBarRightView() {
        TextView tvTitle = (TextView) (getActivity()).findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.registeredBot));
        TextView tvSave;
        tvSave = (TextView) (getActivity()).findViewById(R.id.tvDone);
        tvSave.setVisibility(View.GONE);
        ImageView imageAdd = (ImageView) (getActivity()).findViewById(R.id.imageAdd);
        imageAdd.setVisibility(View.VISIBLE);
        imageAdd.setOnClickListener(this);
    }

    private void callUserInfoAPI() {
        if (!CommonFunctions.isOnline(getActivity())) {
            Toaster.showShortToast(getActivity(), getString(R.string.internet_connection_not_available));
            return;
        }
        progressBuilder.showProgressDialog();
        ApiServiceInterface apiClient = AppClient.getClient().create(ApiServiceInterface.class);
        JsonObject jSon = new JsonObject();
        jSon.addProperty("phone", BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.PHONE_NUMBER, ""));
        apiClient.postUserInfo(jSon).enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                checkUserDetailResponse(response.body());
                progressBuilder.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                progressBuilder.dismissProgressDialog();
                Log.e("Throwable", "Throwable  : " + t.getMessage());
            }
        });
    }

    private void checkUserDetailResponse(UserInfo body) {

        if (null != body && body.getUserDetails().size() > 0) {
            ArrayList<UserDetails> userDetail = body.getUserDetails();
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_FIRST_NAME, userDetail.get(0).first_name).commit();
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_LAST_NAME, userDetail.get(0).last_name).commit();
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_EMAIL, userDetail.get(0).email).commit();
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_ID, userDetail.get(0)._id).commit();

            ((HomeActivity) getActivity()).setNameAndImage();

            if (null != body.getUserDetails().get(0).bot_registered) {

                if (userDetail.get(0).bot_registered.size() > 0) {
                    BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.IS_BOT_REGISTER, "Yes").commit();
                    ArrayList<String> selectedBotIds = new ArrayList<String>();
                    for (int i = 0; i < userDetail.get(0).bot_registered.size(); i++) {
                        selectedBotIds.add(userDetail.get(0).bot_registered.get(i).id);
                    }
                    BotKitApp.saveBotList(selectedBotIds);

                }

                botDetails = body.getUserDetails().get(0).bot_registered;
                adapter = new BotListAdapter(getActivity(), botDetails, "list");
                botListView.setAdapter(adapter);


            }
        }
        setEmptyView();
    }

    private void setEmptyView() {
        if (null != botDetails && botDetails.size() > 0)
            emptyView.setVisibility(View.GONE);
        else
            emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        System.gc();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, new BotListFragment());
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

}
