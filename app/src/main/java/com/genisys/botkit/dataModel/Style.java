package com.genisys.botkit.dataModel;

/**
 * Created by vishwanathd on 03-05-2018.
 */

public class Style {
    public String title_bar_color;
    public String title_bar_text_color;
    public String button_color;
    public String button_font_color;
    public ChatStyle chat_style;
public BgColor bg_color;
    public class ChatStyle{
        public  String user_chat_bubble_bg_color;
        public  String bot_chat_bubble_bg_color;
        public  String bot_linkfy_text_color;
        public  String user_linkfy_text_color;
        public String bot_msg_text_color;
        public  String user_msg_text_color;
        public  String bot_title_text_color;
        public  String user_title_text_color;
    }
}
