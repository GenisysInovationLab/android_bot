package com.genisys.botkit.dataModel;

import com.genisys.botkit.api.pojo.TextMessage;

import java.util.ArrayList;

/**
 * Created by vishwanathd on 11-12-2017.
 */

public class MessageData {

    public int msgType, imgSrc;
    public String msg;
    public String timeMsg;
    public String msgFrom;
    public String imageLink,fileType,fileLink;
    String device;
    String actionType;
    String botType;
    public String mCallbackAction;
    public String mClickMessage;
    public String megText;
    public String mBotName;
    public ArrayList<TextMessage> hybridMsg;

    //MessageData for text/image
    public MessageData(int type, String text, String massageFrom, String tmMsg, int data, String imgLing,String mbotName) {
        this.msgType = type;
        this.msg = text;
        this.timeMsg = tmMsg;
        this.msgFrom = massageFrom;
        this.imgSrc = data;
        this.imageLink = imgLing;
        this.mBotName=mbotName;

    }
    //
    //MessageData for file
    public MessageData(int type, String text, String massageFrom, String tmMsg, String mFileType, String mFileLink,String mbotName) {
        this.msgType = type;
        this.msg = text;
        this.timeMsg = tmMsg;
        this.msgFrom = massageFrom;
        this.fileType = mFileType;
        this.fileLink = mFileLink;
        this.mBotName=mbotName;

    }
//MessageData for buttons
    public MessageData(int type, String txtmsg,String text, String callbackAction, String clickMessage, String tmMsg, String massageFrom,String mbotName) {
       this.megText=txtmsg;
        this.msgType = type;
        this.msg = text;
        this.timeMsg = tmMsg;
        this.msgFrom = massageFrom;
        this.mCallbackAction = callbackAction;
        this.mClickMessage = clickMessage;
        this.mBotName=mbotName;
    }

    public MessageData(int type, ArrayList<TextMessage> message) {
        this.msgType = type;
        this.hybridMsg = message;
    }


}
