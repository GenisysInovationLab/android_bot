package com.genisys.botkit.dataModel;

import com.genisys.botkit.api.pojo.TextMessage;

import java.util.ArrayList;

/**
 * Created by vishwanathd on 27-12-2017.
 */

public class ChatMessage{
    public String userId;
    public ArrayList<TextMessage> message;
    public ChatMessage(String mUserId,ArrayList<TextMessage> msg){
         this.userId=mUserId;
         this.message=msg;
    }

}
