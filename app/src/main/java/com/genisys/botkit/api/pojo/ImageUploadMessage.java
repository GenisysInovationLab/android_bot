package com.genisys.botkit.api.pojo;

/**
 * Created by vishwanathd on 08-01-2018.
 */

public class ImageUploadMessage {
    public String type, image;
    public ImageUploadMessage(String mType, String mText) {
        this.type = mType;
        this.image = mText;
    }
}

