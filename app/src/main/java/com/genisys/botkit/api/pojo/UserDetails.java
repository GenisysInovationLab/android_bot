package com.genisys.botkit.api.pojo;

import java.util.ArrayList;

/**
 * Created by vishwanathd on 04-12-2017.
 */

public class UserDetails  {
    public ArrayList<BotDetail> bot_registered;
    public String _id;
    public String first_name;
    public String last_name;
    public String phone;
    public String email;
    public String dob;
    public String gender;
}
