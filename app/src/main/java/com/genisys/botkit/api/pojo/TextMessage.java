package com.genisys.botkit.api.pojo;

import java.util.ArrayList;

/**
 * Created by vishwanathd on 04-01-2018.
 */
public class TextMessage  {
    public String type, text,video,image,filelink,filetype;
    //public ImageMessage image;
    public ArrayList<ButtonMessage> button;
    public TextMessage(String mType, String mText) {
        this.type = mType;
        this.text = mText;
    }



    public static class ButtonMessage{
        public String text,callbackAction,clickMessage;

        public ButtonMessage(String s, String s1) {
            this.text=s;
            this.callbackAction=s1;
        }
        public ButtonMessage(String s, String s1,String m) {
            this.text=s;
            this.callbackAction=s1;
            this.clickMessage=m;
        }

    }




}
