package com.genisys.botkit.api.pojo;

import java.util.List;

/**
 * Created by vishwanathd on 15-01-2018.
 */

public class Form {
    public String id;
    public String name;
    public String submitTo;
    public List<Field> field;
    //    private Logo logo;
    public Title title;
    public Theme theme;

    public class Theme {
        public String bg_color;
        public String font_size;
        public String title_text_font_color;
        public String title_text_bg_color;
        public String text_box_font_color;
        public String text_box_bg_color;
        public String button_color;
        public String button_text_color;
        public String bg_imgae_url;
        public String logo_image_url;
    }

    public class Title {
        public String type;
        public String name;

        public String alain;
        public String label;
        public String fontSize;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubmitTo() {
        return submitTo;
    }

    public void setSubmitTo(String submitTo) {
        this.submitTo = submitTo;
    }

    public List<Field> getField() {
        return field;
    }

    public void setField(List<Field> field) {
        this.field = field;
    }


}
