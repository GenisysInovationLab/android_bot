package com.genisys.botkit.api.pojo;

/**
 * Created by vishwanathd on 04-01-2018.
 */

public class GenericChatMessage<T> {
    public String userId;
    public String botId;
    public T message;
    public GenericChatMessage(String mUserId, T msg){
        this.userId=mUserId;
        this.message=msg;
    }

}
