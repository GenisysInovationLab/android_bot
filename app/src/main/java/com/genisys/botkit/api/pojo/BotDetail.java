package com.genisys.botkit.api.pojo;

import com.genisys.botkit.dataModel.Style;

import java.io.Serializable;

/**
 * Created by vishwanathd on 04-12-2017.
 */

public class BotDetail implements Serializable {
    public String id;
    public boolean isSelected;
    public String name;
    public String description;
    public String photo_url;
    public String totalUsers;
    public String createdOn;
    public String owner;
    public Style style;
    public String ispublic;

}
