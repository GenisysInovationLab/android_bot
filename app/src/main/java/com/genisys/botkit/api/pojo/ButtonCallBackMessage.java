package com.genisys.botkit.api.pojo;

/**
 * Created by vishwanathd on 12-01-2018.
 */

public class ButtonCallBackMessage {
    public String type, callbackAction,cardText;

    public ButtonCallBackMessage(String mType, String mCallbackAction,String mCardText) {
        this.type = mType;
        this.callbackAction = mCallbackAction;
        this.cardText=mCardText;
    }
    public ButtonCallBackMessage(String mType, String mCallbackAction) {
        this.type = mType;
        this.callbackAction = mCallbackAction;
    }
}
