package com.genisys.botkit.api.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vishwanathd on 16-11-2017.
 */

public class OtpVerify {
    public String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("user_status")
    @Expose
    private String userStatus;

    @SerializedName("user_details")
    @Expose
    private ArrayList<UserDetails> userDetails;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
    public ArrayList<UserDetails> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(ArrayList<UserDetails> userDetails) {
        this.userDetails = userDetails;
    }
}
