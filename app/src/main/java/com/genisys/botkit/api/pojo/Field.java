package com.genisys.botkit.api.pojo;

import com.genisys.botkit.widgets.GuiEditBox;
import com.genisys.botkit.widgets.GuiPickOne;

import java.util.List;

/**
 * Created by vishwanathd on 15-01-2018.
 */


public class Field {
    public String name;
    public String type;
    public String label;
    public String required;
    public List<String> options ;
    public Object obj;

    public Object getData()
    {
        if (type.equals("text") || type.equals("numeric"))
        {
            if (obj != null) {
                GuiEditBox b = (GuiEditBox) obj;
                return b.getValue();
            }
        }
        if (type.equals("choice")) {
            if (obj != null) {
                GuiPickOne po = (GuiPickOne) obj;
                return po.getValue();
            }
        }

        // todo, add other UI elements here
        return null;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }


}
