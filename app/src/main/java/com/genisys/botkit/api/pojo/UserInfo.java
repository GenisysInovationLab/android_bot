package com.genisys.botkit.api.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vishwanathd on 05-12-2017.
 */

public class UserInfo {
    @SerializedName("user_details")
    @Expose
    private ArrayList<UserDetails> userDetails;

    public ArrayList<UserDetails> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(ArrayList<UserDetails> userDetails) {
        this.userDetails = userDetails;
    }
}
