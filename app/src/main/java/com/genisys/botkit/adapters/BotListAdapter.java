package com.genisys.botkit.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.genisys.botkit.R;
import com.genisys.botkit.activities.ChatMessageActivity;
import com.genisys.botkit.api.pojo.BotDetail;
import com.genisys.botkit.util.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by vishwanathd on 04-12-2017.
 */

public class BotListAdapter extends BaseAdapter {
    private Context mContext;
    public ArrayList<BotDetail> mList;
    private LayoutInflater inflater;
    private int mSelectedItem;
    private String mtype;


    public BotListAdapter(Context context, ArrayList<BotDetail> botLists, String type) {
        mContext = context;
//        this.mList.addAll(botLists);
        mList = botLists;
        mtype = type;
        if (context != null) {

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public int getCount() {

        return mList.size();
    }

    @Override
    public BotDetail getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderBotList viewHolder;
        final BotDetail botList = mList.get(position);
        Log.i("Bot Name", botList.name);

        if (convertView == null) {
            viewHolder = new ViewHolderBotList();
            convertView = inflater.inflate(R.layout.bot_list_items, null);
            viewHolder.tvBotName = (TextView) convertView.findViewById(R.id.tvBotNames);
            viewHolder.ivBotImage = (ImageView) convertView.findViewById(R.id.ivBot);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.cbBot);
            viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    BotDetail _detail=(BotDetail)cb.getTag();
                    _detail.isSelected=cb.isChecked();
                }
            });
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderBotList) convertView.getTag();
        }
        viewHolder.tvBotName.setText(botList.name);

        if (mtype.equalsIgnoreCase("list")) {
            viewHolder.checkBox.setVisibility(View.GONE);
        } else if (mtype.equalsIgnoreCase("reg")) {
            Log.i("Bot isSelected", ": " + botList.isSelected);
            viewHolder.checkBox.setVisibility(View.VISIBLE);
            viewHolder.checkBox.setChecked(botList.isSelected);
        }
//        if (botList.name.equalsIgnoreCase("Travel Bot"))
//            viewHolder.ivBotImage.setImageResource(R.drawable.travel);
//        else {
//            viewHolder.ivBotImage.setImageResource(R.drawable.genibot);
//        }
        Log.i("photo","bot.name "+ botList.name+" : "+botList.photo_url);
if(!botList.photo_url.trim().equalsIgnoreCase("")){

//    Picasso.with(this.mContext).load(botList.photo_url).fit().centerCrop()
//            .placeholder(R.drawable.genibot)
//            .error(R.drawable.genibot)
//            .into(viewHolder.ivBotImage);
//

    Picasso.with(this.mContext).load(botList.photo_url).resize(100, 100).centerCrop()
            .transform(new CircleTransform()).placeholder(R.drawable.genibot).into(viewHolder.ivBotImage, new Callback() {

        @Override
        public void onSuccess() {
        }

        @Override
        public void onError() {
        }
    });

//   Glide.with(this.mContext)
//            .load(botList.photo_url).thumbnail(0.4f)
//            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .override(100, 100)
//            .placeholder(R.drawable.genibot)
//            .centerCrop()
//            .into(viewHolder.ivBotImage);
}else if(botList.photo_url.trim().equalsIgnoreCase("")){

    viewHolder.ivBotImage.setImageResource(R.drawable.genibot);
}

        viewHolder.checkBox.setChecked(botList.isSelected);
        viewHolder.checkBox.setTag(botList);
        return convertView;
    }

    private class ViewHolderBotList {
        TextView tvBotName;
        ImageView ivBotImage;
        CheckBox checkBox;

    }
}
