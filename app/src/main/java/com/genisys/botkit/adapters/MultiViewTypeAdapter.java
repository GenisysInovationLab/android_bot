package com.genisys.botkit.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.dataModel.MessageData;
import com.genisys.botkit.dataModel.RanderType;
import com.genisys.botkit.dataModel.Style;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.ImageBlur;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MultiViewTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MessageData> dataSet;
    Context mContext;
    int total_types;
    MediaPlayer mPlayer;
    private boolean fabStateVolume = false;
    String selectedBotId;
    public MultiViewTypeAdapterListener onClickListener;

    public interface MultiViewTypeAdapterListener {
        void buttonTypeButtonViewOnClick(View v, int position);

        void imageTypeImageViewOnClick(View v, int position);

        void textTypeTextViewOnClick(String clickedTxt);
    }

    Pattern urlPatern;

    public class TextTypeViewHolder extends RecyclerView.ViewHolder {
        TextView bot_name, bot_message_text, bot_time_text;
        TextView my_name, my_message_text, my_time_text;
        RelativeLayout rlBotMessage, rlMyMessage;
        public int botLinkfyColor;
        // CardView cardView;

        public TextTypeViewHolder(View itemView) {
            super(itemView);

            this.bot_message_text = (TextView) itemView.findViewById(R.id.bot_message_text);
            this.bot_name = (TextView) itemView.findViewById(R.id.bot_name);
            this.bot_time_text = (TextView) itemView.findViewById(R.id.bot_time_text);
            this.rlBotMessage = (RelativeLayout) itemView.findViewById(R.id.rlBotMessage);
            this.rlMyMessage = (RelativeLayout) itemView.findViewById(R.id.rlMyMessage);
            this.my_name = (TextView) itemView.findViewById(R.id.my_name);
            this.my_message_text = (TextView) itemView.findViewById(R.id.my_message_text);
            this.my_time_text = (TextView) itemView.findViewById(R.id.my_time_text);
            /**
             * Text Chat type style/theme applying
             */
            Style styleObj = BotKitApp.getBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME + selectedBotId);
            if (null != styleObj && null != styleObj.chat_style) {
                Log.i("user_chat_bubble_bg_color", ": " + styleObj.chat_style.user_chat_bubble_bg_color);
                Log.i("bot_chat_bubble_bg_color", ": " + styleObj.chat_style.bot_chat_bubble_bg_color);

                if ((null != styleObj.chat_style.user_chat_bubble_bg_color && !styleObj.chat_style.user_chat_bubble_bg_color.equalsIgnoreCase("")) && (null != styleObj.chat_style.bot_chat_bubble_bg_color && !styleObj.chat_style.bot_chat_bubble_bg_color.equalsIgnoreCase(""))) {
                    /**
                     *  nine patch image color change
                     */
                    Drawable mDrawable2 = mContext.getResources().getDrawable(R.drawable.balloon_outgoing_normal).mutate();
                    mDrawable2.setColorFilter(new PorterDuffColorFilter(Color.parseColor(styleObj.chat_style.user_chat_bubble_bg_color), PorterDuff.Mode.SRC_IN));
                    this.rlMyMessage.setBackground(mDrawable2);

                    Drawable mDrawable = mContext.getResources().getDrawable(R.drawable.balloon_incoming_normal).mutate();
                    mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(styleObj.chat_style.bot_chat_bubble_bg_color), PorterDuff.Mode.SRC_IN));
                    this.rlBotMessage.setBackground(mDrawable);
                }

                if (null != styleObj.chat_style.bot_msg_text_color && !styleObj.chat_style.bot_msg_text_color.equalsIgnoreCase("")) {
                    this.bot_message_text.setTextColor(Color.parseColor(styleObj.chat_style.bot_msg_text_color));
                }
                if (null != styleObj.chat_style.user_msg_text_color && !styleObj.chat_style.user_msg_text_color.equalsIgnoreCase("")) {
                    this.my_message_text.setTextColor(Color.parseColor(styleObj.chat_style.user_msg_text_color));
                }
                if (null != styleObj.chat_style.bot_title_text_color && !styleObj.chat_style.bot_title_text_color.equalsIgnoreCase("")) {
                    this.bot_name.setTextColor(Color.parseColor(styleObj.chat_style.bot_title_text_color));
                }
                if (null != styleObj.chat_style.user_title_text_color && !styleObj.chat_style.user_title_text_color.equalsIgnoreCase("")) {
                    this.my_name.setTextColor(Color.parseColor(styleObj.chat_style.user_title_text_color));
                }

                if (null != styleObj.chat_style.bot_linkfy_text_color && !styleObj.chat_style.bot_linkfy_text_color.equalsIgnoreCase("")) {
                    this.botLinkfyColor = Color.parseColor(styleObj.chat_style.bot_linkfy_text_color);
                } else {
                    this.botLinkfyColor = Color.parseColor("#355da7");
                }

                if (null != styleObj.chat_style.user_linkfy_text_color && !styleObj.chat_style.user_linkfy_text_color.equalsIgnoreCase("")) {
                    this.my_message_text.setLinkTextColor(Color.parseColor(styleObj.chat_style.user_linkfy_text_color));
                } else {
                    this.my_message_text.setLinkTextColor(this.botLinkfyColor = Color.parseColor("#355da7"));
                }


            } else {
                /**
                 * Text Chat type bot Linkfy Color for bot text
                 */
                this.botLinkfyColor = Color.parseColor("#355da7");
            }

            //this.cardView = (CardView) itemView.findViewById(R.id.card_view);
        }

    }

    public class ImageTypeViewHolder extends RecyclerView.ViewHolder {


        TextView my_img_text, my_name, my_time_text;
        TextView bot_img_text, bot_name, bot_time_text;
        ImageView my_img, bot_img;
        RelativeLayout rlMyImgTxtMsg, rlBotImgTextMsg;

        public ImageTypeViewHolder(View itemView) {
            super(itemView);
            this.my_name = (TextView) itemView.findViewById(R.id.my_name);
            this.my_img_text = (TextView) itemView.findViewById(R.id.my_img_text);
            this.my_time_text = (TextView) itemView.findViewById(R.id.my_time_text);
            this.my_img = (ImageView) itemView.findViewById(R.id.my_img);
            this.rlMyImgTxtMsg = (RelativeLayout) itemView.findViewById(R.id.rlMyImgTxtMsg);

            this.bot_name = (TextView) itemView.findViewById(R.id.bot_name);
            this.bot_img_text = (TextView) itemView.findViewById(R.id.bot_img_text);
            this.bot_time_text = (TextView) itemView.findViewById(R.id.bot_time_text);
            this.bot_img = (ImageView) itemView.findViewById(R.id.bot_img);
            this.rlBotImgTextMsg = (RelativeLayout) itemView.findViewById(R.id.rlBotImgTextMsg);


            /**
             * Text/Image type style/theme applying for bubble bg
             */
            Style styleObj = BotKitApp.getBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME + selectedBotId);
            if (null != styleObj && null != styleObj.chat_style) {
                Log.i("user_chat_bubble_bg_color", ": " + styleObj.chat_style.user_chat_bubble_bg_color);
                Log.i("bot_chat_bubble_bg_color", ": " + styleObj.chat_style.bot_chat_bubble_bg_color);

                if ((null != styleObj.chat_style.user_chat_bubble_bg_color && !styleObj.chat_style.user_chat_bubble_bg_color.equalsIgnoreCase("")) && (null != styleObj.chat_style.bot_chat_bubble_bg_color && !styleObj.chat_style.bot_chat_bubble_bg_color.equalsIgnoreCase(""))) {
                    /**
                     *  nine patch image color change
                     */
                    Drawable mDrawable2 = mContext.getResources().getDrawable(R.drawable.balloon_outgoing_normal).mutate();
                    mDrawable2.setColorFilter(new PorterDuffColorFilter(Color.parseColor(styleObj.chat_style.user_chat_bubble_bg_color), PorterDuff.Mode.SRC_IN));
                    this.rlMyImgTxtMsg.setBackground(mDrawable2);

                    Drawable mDrawable = mContext.getResources().getDrawable(R.drawable.balloon_incoming_normal).mutate();
                    mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(styleObj.chat_style.bot_chat_bubble_bg_color), PorterDuff.Mode.SRC_IN));
                    this.rlBotImgTextMsg.setBackground(mDrawable);
                }

                if (null != styleObj.chat_style.bot_msg_text_color && !styleObj.chat_style.bot_msg_text_color.equalsIgnoreCase("")) {
                    this.bot_img_text.setTextColor(Color.parseColor(styleObj.chat_style.bot_msg_text_color));
                }
                if (null != styleObj.chat_style.user_msg_text_color && !styleObj.chat_style.user_msg_text_color.equalsIgnoreCase("")) {
                    this.my_img_text.setTextColor(Color.parseColor(styleObj.chat_style.user_msg_text_color));
                }

                if (null != styleObj.chat_style.bot_title_text_color && !styleObj.chat_style.bot_title_text_color.equalsIgnoreCase("")) {
                    this.bot_name.setTextColor(Color.parseColor(styleObj.chat_style.bot_title_text_color));
                }
                if (null != styleObj.chat_style.user_title_text_color && !styleObj.chat_style.user_title_text_color.equalsIgnoreCase("")) {
                    this.my_name.setTextColor(Color.parseColor(styleObj.chat_style.user_title_text_color));
                }

            }

            this.bot_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.imageTypeImageViewOnClick(view, getAdapterPosition());
                }
            });
        }

    }

    public class HybridTypeViewHolder extends RecyclerView.ViewHolder {

        HorizontalScrollView horizontalScrollView;
        LinearLayout llContainer;
        RelativeLayout container;

        public HybridTypeViewHolder(View itemView) {
            super(itemView);
            // this.container=(RelativeLayout)itemView.findViewById(R.id.hsv);

            this.horizontalScrollView = (HorizontalScrollView) itemView.findViewById(R.id.hsv);
            this.llContainer = (LinearLayout) itemView.findViewById(R.id.llContainer);

        }
    }

    public class ButtonTypeViewHolder extends RecyclerView.ViewHolder {
        Button bot_btn;
        TextView bot_name, bot_btn_text, bot_time_text;

        public ButtonTypeViewHolder(View itemView) {
            super(itemView);
            this.bot_btn = (Button) itemView.findViewById(R.id.bot_btn);
            this.bot_btn_text = itemView.findViewById(R.id.bot_btn_text);

            /**
             * Button Chat type style/theme applying
             */
            Style buttonStyle = BotKitApp.getBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME + selectedBotId);
            if (null != buttonStyle) {
                if (null != buttonStyle.button_color && !buttonStyle.button_color.equalsIgnoreCase(""))
                    this.bot_btn.setBackgroundColor(Color.parseColor(buttonStyle.button_color));
                if (null != buttonStyle.button_font_color && !buttonStyle.button_font_color.equalsIgnoreCase(""))
                    this.bot_btn.setTextColor(Color.parseColor(buttonStyle.button_font_color));
            }
            this.bot_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.buttonTypeButtonViewOnClick(view, getAdapterPosition());
                }
            });
        }
    }

    Transformation blurTransformation = new Transformation() {
        @Override
        public Bitmap transform(Bitmap source) {
            Bitmap blurred = ImageBlur.fastblur(MultiViewTypeAdapter.this.mContext, source, 10);
            source.recycle();
            return blurred;
        }

        @Override
        public String key() {
            return "blur()";
        }
    };

    public MultiViewTypeAdapter(ArrayList<MessageData> data, Context context, MultiViewTypeAdapterListener listener, String sBotId) {
        this.dataSet = data;
        this.mContext = context;
        this.onClickListener = listener;
        this.selectedBotId = sBotId;
        this.urlPatern = Pattern.compile(
                "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                        + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                        + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case RanderType.TEXT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text_type, parent, false);
                return new TextTypeViewHolder(view);
            case RanderType.IMAGE_TYPE:
            case RanderType.FILE_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_type, parent, false);
                return new ImageTypeViewHolder(view);
            case RanderType.BUTTON_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text_button_type, parent, false);
                return new ButtonTypeViewHolder(view);
            case RanderType.PAGER_TYPE:
                // createHybridView(parent);
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hybrid_layout, parent, false);
                return new HybridTypeViewHolder(view);

        }
        return null;


    }


    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).msgType) {
            case 0:
                return RanderType.TEXT_TYPE;
            case 1:
                return RanderType.IMAGE_TYPE;
            case 2:
                return RanderType.PAGER_TYPE;
            case 3:
                return RanderType.BUTTON_TYPE;
            case 4:
                return RanderType.FILE_TYPE;
            default:
                return -1;
        }


    }

    public Spannable setSpan(String text, int color) {
        Spannable span = new SpannableString(text);

        final Matcher matcher = Pattern.compile(".*(/help|/domains|/services).*").matcher(text);


        while (matcher.find()) {
            final String botCmd = matcher.group(1);

            int botCmdIndex = text.indexOf(botCmd) - 1;
            span.setSpan(new SpanListener(botCmd, color), botCmdIndex, botCmdIndex + botCmd.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//            final String webUrl=matcher.group(2);
//            int webUrlIndex=text.indexOf(webUrl)-1;
//            span.setSpan(new SpanListener(webUrl), webUrlIndex, webUrlIndex + webUrl.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return span;
    }

    public class SpanListener extends URLSpan {
        String spanned_text;
        int mColor;

        public SpanListener(String text, int color) {
            super(text);
            spanned_text = text;
            mColor = color;
        }

        public void onClick(View v) {

            Log.i("clicked text", spanned_text);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.textTypeTextViewOnClick(spanned_text);
                    Log.i("clicked text", spanned_text);
                }
            });
        }

        @Override
        public void updateDrawState(TextPaint paint) {
            super.updateDrawState(paint);
            //Remove underline
            paint.setColor(mColor);
            paint.setUnderlineText(true);

        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        MessageData object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.msgType) {
                case RanderType.TEXT_TYPE:
                    ((TextTypeViewHolder) holder).bot_message_text.setText(object.msg);
                    if (object.msgFrom.equalsIgnoreCase("me")) {
                        ((TextTypeViewHolder) holder).rlMyMessage.setVisibility(View.VISIBLE);
                        ((TextTypeViewHolder) holder).rlBotMessage.setVisibility(View.GONE);
                        ((TextTypeViewHolder) holder).my_name.setText("me");
                        ((TextTypeViewHolder) holder).my_message_text.setText(object.msg);
                        ((TextTypeViewHolder) holder).my_time_text.setText(object.timeMsg);
                        // ((TextTypeViewHolder) holder).my_message_text.setLinkTextColor(Color.parseColor("#355da7"));

                    } else if (object.msgFrom.equalsIgnoreCase("bot")) {
                        ((TextTypeViewHolder) holder).rlMyMessage.setVisibility(View.GONE);
                        ((TextTypeViewHolder) holder).rlBotMessage.setVisibility(View.VISIBLE);


                        ((TextTypeViewHolder) holder).bot_name.setText(object.mBotName + " bot");
                        Matcher mMatcher = this.urlPatern.matcher(object.msg);
                        boolean matchFound = false;
                        while (mMatcher.find()) {
                            int matchStart = mMatcher.start(1);
                            int matchEnd = mMatcher.end();
                            matchFound = true;
                            break;
                            // now you have the offsets of a URL match
                        }
                        if (matchFound) {
                            System.out.println("String contains URL");
                            Linkify.addLinks(((TextTypeViewHolder) holder).bot_message_text, Linkify.ALL);
                            ((TextTypeViewHolder) holder).bot_message_text.setLinkTextColor(((TextTypeViewHolder) holder).botLinkfyColor);//#355da7"
                        } else if (!matchFound) {
                            ((TextTypeViewHolder) holder).bot_message_text.setText(setSpan(object.msg, ((TextTypeViewHolder) holder).botLinkfyColor), TextView.BufferType.SPANNABLE);
                            ((TextTypeViewHolder) holder).bot_message_text.setMovementMethod(LinkMovementMethod.getInstance());


                        }
                        //((TextTypeViewHolder) holder).bot_message_text.setText(object.msg);
                        ((TextTypeViewHolder) holder).bot_time_text.setText(object.timeMsg);
                    }

                    break;
                case RanderType.IMAGE_TYPE:
                case RanderType.FILE_TYPE:
                    if (object.msgFrom.equalsIgnoreCase("me")) {
                        ((ImageTypeViewHolder) holder).rlMyImgTxtMsg.setVisibility(View.VISIBLE);
                        ((ImageTypeViewHolder) holder).rlBotImgTextMsg.setVisibility(View.GONE);
                        ((ImageTypeViewHolder) holder).my_name.setText("me");
                        ((ImageTypeViewHolder) holder).my_img_text.setText(object.msg);
                        ((ImageTypeViewHolder) holder).my_time_text.setText(object.timeMsg);
                        if (object.imageLink != null & object.imageLink.length() > 0)
                            Picasso.with(mContext).load(new File(object.imageLink)).resize(240, 240).centerCrop().placeholder(R.drawable.image_issue).into(((ImageTypeViewHolder) holder).my_img);
                        else
                            ((ImageTypeViewHolder) holder).my_img.setImageResource(object.imgSrc);
                    } else if (object.msgFrom.equalsIgnoreCase("bot")) {
                        ((ImageTypeViewHolder) holder).rlBotImgTextMsg.setVisibility(View.VISIBLE);
                        ((ImageTypeViewHolder) holder).rlMyImgTxtMsg.setVisibility(View.GONE);
                        ((ImageTypeViewHolder) holder).bot_name.setText(object.mBotName + " bot");
                        ((ImageTypeViewHolder) holder).bot_img_text.setText(object.msg);
                        ((ImageTypeViewHolder) holder).bot_time_text.setText(object.timeMsg);
                        // ((ImageTypeViewHolder) holder).bot_img.setImageResource(object.imgSrc);

                        if (object.msgType == RanderType.FILE_TYPE) {
                            int imageSrc;
                            if (object.fileType.equalsIgnoreCase("doc"))
                                imageSrc = R.drawable.doc;
                            else if (object.fileType.equalsIgnoreCase("ppt"))
                                imageSrc = R.drawable.ppt;
                            else if (object.fileType.equalsIgnoreCase("xls"))
                                imageSrc = R.drawable.xls;
                            else
                                imageSrc = R.drawable.pdf;

                            ((ImageTypeViewHolder) holder).bot_img.setImageResource(imageSrc);


                        } else {
                            if (object.imageLink != null & object.imageLink.length() > 0)
                                Picasso.with(mContext).load(object.imageLink).resize(240, 240).centerCrop().placeholder(R.drawable.image_issue).into(((ImageTypeViewHolder) holder).bot_img);
                            else
                                ((ImageTypeViewHolder) holder).bot_img.setImageResource(object.imgSrc);

                        }
                    }
                    break;
                case RanderType.BUTTON_TYPE:

                    ((ButtonTypeViewHolder) holder).bot_btn_text.setText(object.megText);
                    if (object.megText.equalsIgnoreCase("")) {
                        ((ButtonTypeViewHolder) holder).bot_btn_text.setVisibility(View.GONE);
                    } else {
                        ((ButtonTypeViewHolder) holder).bot_btn_text.setVisibility(View.VISIBLE);

                    }
                    ((ButtonTypeViewHolder) holder).bot_btn.setText(object.msg);
                    break;
                case RanderType.PAGER_TYPE:

//                    HorizontalScrollView horizontalScrollView=new HorizontalScrollView(mContext);
//                    LinearLayout.LayoutParams horizontalScrollViewparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//                    horizontalScrollView.setLayoutParams(horizontalScrollViewparams);
//
//                    LinearLayout llContainer=new LinearLayout(mContext);
//                    LinearLayout.LayoutParams llContainerparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//                    llContainer.setOrientation(LinearLayout.HORIZONTAL);
//                    llContainer.setLayoutParams(llContainerparams);

                    int btnBgColor = Color.parseColor("#277C88");
                    int btnTxtColor = Color.parseColor("#FFFFFF");
                    //TODO button theme
//                    if(!BotKitApp.getAppTheme().equalsIgnoreCase(AppConstants.PREFERENCES.APP_THEME_DEFAULT_VALUE)) {
//                        btnBgColor=Color.parseColor("#FF0000");
//                        btnTxtColor=Color.parseColor("#FFFFFF");
//                        }
                    for (int i = 0; i < object.hybridMsg.size(); i++) {
                        CardView card = new CardView(mContext);
                        // Set the CardView layoutParams
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        card.setLayoutParams(params);
                        // Set CardView corner radius
                        card.setRadius(CommonFunctions.convertDpToPx(9));
                        // Set cardView content padding
                        card.setContentPadding(15, 15, 15, 15);
                        // Set a background color for CardView
                        card.setCardBackgroundColor(Color.parseColor("#FFC6D6C3"));
                        // Set the CardView maximum elevation
                        card.setMaxCardElevation(CommonFunctions.convertDpToPx(15));
                        // Set CardView elevation
                        card.setCardElevation(CommonFunctions.convertDpToPx(9));

                        LinearLayout linearLayout_907 = new LinearLayout(mContext);
                        linearLayout_907.setOrientation(LinearLayout.VERTICAL);
                        linearLayout_907.setPaddingRelative(CommonFunctions.convertDpToPx(5), CommonFunctions.convertDpToPx(5), CommonFunctions.convertDpToPx(5), CommonFunctions.convertDpToPx(5));
                        LinearLayout.LayoutParams layout_371 = new LinearLayout.LayoutParams(CommonFunctions.convertDpToPx(300), LinearLayout.LayoutParams.WRAP_CONTENT);
                        linearLayout_907.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        layout_371.setMargins(CommonFunctions.convertDpToPx(10), CommonFunctions.convertDpToPx(10), CommonFunctions.convertDpToPx(10), CommonFunctions.convertDpToPx(10));
                        linearLayout_907.setLayoutParams(layout_371);

                        ImageView imageView_964 = new ImageView(mContext);
                        LinearLayout.LayoutParams layout_851 = new LinearLayout.LayoutParams(CommonFunctions.convertDpToPx(250), CommonFunctions.convertDpToPx(250));
                        layout_851.gravity = Gravity.CENTER;
                        //imageView_964.setImageResource(R.drawable.image_issue);
                        Picasso.with(mContext).load(object.hybridMsg.get(i).image).resize(250, 250).centerCrop().placeholder(R.drawable.image_issue).into(imageView_964);

                        imageView_964.setLayoutParams(layout_851);
                        linearLayout_907.addView(imageView_964);


                        TextView textView_818 = new TextView(mContext);
                        textView_818.setTextColor(Color.BLACK);
                        LinearLayout.LayoutParams layout_272 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        textView_818.setLayoutParams(layout_272);
                        textView_818.setText(object.hybridMsg.get(i).text);
                        linearLayout_907.addView(textView_818);

                        for (int j = 0; j < object.hybridMsg.get(i).button.size(); j++) {

                            Button button_444 = new Button(mContext);
                            button_444.setBackgroundColor(btnBgColor);
                            LinearLayout.LayoutParams layout_977 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, CommonFunctions.convertDpToPx(50));
                            button_444.setLayoutParams(layout_977);
                            button_444.setTextColor(btnTxtColor);
                            layout_977.setMargins(0, 0, 0, CommonFunctions.convertDpToPx(5));
                            button_444.setText(object.hybridMsg.get(i).button.get(j).text);
                            linearLayout_907.addView(button_444);
                        }

                        card.addView(linearLayout_907);
                        //  llContainer.addView(card);
                        (((HybridTypeViewHolder) holder).llContainer).addView(card);

                    }

//                    horizontalScrollView.addView(llContainer);
//                    ((HybridTypeViewHolder)holder).container.addView(horizontalScrollView);
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {

        return dataSet.size();
    }


}
