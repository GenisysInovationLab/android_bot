package com.genisys.botkit;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.genisys.botkit.dataModel.Style;
import com.genisys.botkit.network.volley.DataManager;
import com.genisys.botkit.util.ApiConstants;
import com.genisys.botkit.util.AppConstants;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vishwanathd on 01-12-2017.
 */

public class BotKitApp extends Application {
    public static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String phoneNumber;
    private static BotKitApp instance;
    @Override
    public void onCreate() {

//        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
//        }
       
        super.onCreate();

        instance = this;
        sharedPreferences = getSharedPreferences("BOT_USER_INFO", 0);
        editor = sharedPreferences.edit();



    }


    public static SharedPreferences.Editor editSharePrefs() {
        return editor;
    }

    public static BotKitApp getAPP() {
        return instance;
    }

    public static synchronized BotKitApp getInstance() {
        return instance;
    }
    public static Point getScreenSize() {
        final WindowManager wm = (WindowManager) BotKitApp.getAPP().getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        return size;
    }
    public static void saveBotList(ArrayList<String> botids){
        Set<String> set = new HashSet<String>();
        set.addAll(botids);
        editor.putStringSet(AppConstants.PREFERENCES.SAVED_BOT_LIST,set);
        editor.apply();
    }

    public static ArrayList<String> getSavedBotList(){
        ArrayList<String> botList=new ArrayList<String>();
        Set<String> set = sharedPreferences.getStringSet(AppConstants.PREFERENCES.SAVED_BOT_LIST, null);
        if(null!=set)
        botList.addAll(set);
        Log.d("botList",": "+set);
        return botList;
    }
    public static String getImageUrl(){
        return DataManager.GET_IMAGE_DOMAIN+"/"+sharedPreferences.getString(ApiConstants.USERID,"")+"/";
    }

    public static String getAppTheme(){
       // return sharedPreferences.getString(AppConstants.PREFERENCES.APP_THEME,AppConstants.PREFERENCES.APP_THEME_DEFAULT_VALUE);
    return null;
    }
public static void saveBotTheme(String key, Style botStyle){
    Gson gson = new Gson();
    String botStyleStr=gson.toJson(botStyle);
    Log.i("KEY","=================="+key+"======================");
    Log.i("BotStyle",": "+botStyleStr);
    editor.putString(key,botStyleStr);
    editor.apply();
}
public static void removeBotTheme(String key){
if(sharedPreferences.contains(key)){
    BotKitApp.editSharePrefs().remove(key).commit();
    BotKitApp.editSharePrefs().commit();
}
}
public static Style getBotTheme(String key){
    Gson gson=new Gson();
    String json=sharedPreferences.getString(key,null);
    Style styleObj=null;
    if(null!=json) {
        styleObj = gson.fromJson(json, Style.class);
        Log.i("KEY","=================="+key+"======================");
        Log.i("title_bar_color ", ": " + styleObj.title_bar_color);
        Log.i("title_bar_text_color ", ": " + styleObj.title_bar_text_color);
        Log.i("button_color ", ": " + styleObj.button_color);
        Log.i("button_font_color ", ": " + styleObj.button_font_color);
        Log.i("=====bg_color=======", "\nstart_color : " + styleObj.bg_color.start_color + "\ncenter_color: " + styleObj.bg_color.center_color + "\nend_color : " + styleObj.bg_color.end_color);
        Log.i("========chat_style== ", "\nuser_chat_bubble_bg_color: " + styleObj.chat_style.user_chat_bubble_bg_color + "\nbot_chat_bubble_bg_color: " + styleObj.chat_style.bot_chat_bubble_bg_color + "\nbot_linkfy_text_color: " + styleObj.chat_style.bot_linkfy_text_color + "\nuser_linkfy_text_color : " + styleObj.chat_style.user_linkfy_text_color + "\nbot_msg_text_color : " + styleObj.chat_style.bot_msg_text_color + "\nuser_msg_text_color : " + styleObj.chat_style.user_msg_text_color+ "\nbot_title_text_color : " + styleObj.chat_style.bot_title_text_color+ "\nuser_title_text_color : " + styleObj.chat_style.user_title_text_color);
    }
    return styleObj;

}
}
