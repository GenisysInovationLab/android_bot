package com.genisys.botkit.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.util.AppConstants;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setThreadForSplashScreen();
    }

    private void setThreadForSplashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startNextScreen();

            }
        }, 1000);
    }

    private void startNextScreen() {

//        startActivity(new Intent(SplashActivity.this,ChatMessageActivity.class));
//        finish();

//        startActivity(new Intent(SplashActivity.this,DynamicForm.class));
//        finish();

        if(BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.IS_USER_REGISTER,"No").equalsIgnoreCase("Yes"))
        {
            // go to RegisteredBot screen
            startActivity(new Intent(SplashActivity.this,HomeActivity.class));
            finish();
        }else{
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }

        //overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
   }

}
