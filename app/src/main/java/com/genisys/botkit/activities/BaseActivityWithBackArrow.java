package com.genisys.botkit.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.genisys.botkit.R;
import com.genisys.botkit.util.CommonFunctions;

public abstract class BaseActivityWithBackArrow extends AppCompatActivity {

    private  String TAG=BaseActivityWithBackArrow.class.getSimpleName();
    public Toolbar toolbar;
    ActionBar actionBar;
    public abstract int getLayoutResource();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        setToolbarWithBack();
    }
    void setToolbarWithBack() {
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.back_img);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CommonFunctions.hideSoftKeyboard(BaseActivityWithBackArrow.this);
                Log.d(TAG, "home click");
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
