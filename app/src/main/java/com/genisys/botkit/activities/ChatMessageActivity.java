package com.genisys.botkit.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.TestData;
import com.genisys.botkit.adapters.MultiViewTypeAdapter;
import com.genisys.botkit.api.pojo.ButtonCallBackMessage;
import com.genisys.botkit.api.pojo.GenericChatMessage;
import com.genisys.botkit.api.pojo.ImageUploadMessage;
import com.genisys.botkit.api.pojo.TextMessage;
import com.genisys.botkit.api.pojo.UserSignup;
import com.genisys.botkit.cropImage.CropImage;
import com.genisys.botkit.dataModel.ChatMessage;
import com.genisys.botkit.dataModel.MessageData;
import com.genisys.botkit.dataModel.RanderType;
import com.genisys.botkit.dataModel.Style;
import com.genisys.botkit.network.Connectivity;
import com.genisys.botkit.network.volley.DataManager;
import com.genisys.botkit.network.volley.ResultListenerNG;
import com.genisys.botkit.receiver.ConnectivityReceiver;
import com.genisys.botkit.receiver.MyPhoneStateListener;
import com.genisys.botkit.util.ApiConstants;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.AppThemeUtils;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.ProgressBuilder;
import com.genisys.botkit.util.Toaster;
import com.genisys.botkit.widgets.ToolDotProgress;
import com.github.zagum.speechrecognitionview.RecognitionProgressView;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class ChatMessageActivity extends BaseActivityWithBackArrow implements View.OnClickListener, TextWatcher, RecognitionListener, ConnectivityReceiver.ConnectivityReceiverListener, MyPhoneStateListener.ConnectivitySignalListener {
    private TextView toolbar_title;
    private EditText etMessage;
    private ImageView ivSend;
    private ImageView ivAddAttachment;
    private ImageView ivMic;
    private String userId;
    RecyclerView mRecyclerView;
    RelativeLayout layoutPopIV;
    ImageView ib_close;
    ImageView image_preview;
    MultiViewTypeAdapter adapter;
    MultiViewTypeAdapter.MultiViewTypeAdapterListener adapterListener;

    ConnectionFactory factory = new ConnectionFactory();
    private BlockingDeque<String> queue = new LinkedBlockingDeque<String>();
    private static final int REQUEST_CODE_APP_PERMISSIONS = 1;
    Thread subscribeThread;
    Thread publishThread;
    AlertDialog.Builder alert;
    ToolDotProgress dots_progress;
    private ArrayList<MessageData> list;
    public static final int CAMERA_REQUEST = 2;
    public static final int PICK_IMAGE_REQUEST = 3;
    public static final int CROP_IMAGE_REQUEST = 4;
    public static final int REQ_CODE_SPEECH_INPUT = 5;
    public static final int REQUEST_RECORD_PERMISSION = 6;
    public static final int PICKFILE_RESULT_CODE = 7;
    public static final int FORM_REQUEST_CODE = 6;
    public static File _photoFile;
    public String mCurrentPhotoPath = "";
    String selectedBotId, botName;
    int buttonArraySize = 0, buttonStartIndex = 0;
    private static final String TAG = ChatMessageActivity.class.getSimpleName();
    private String CONSUMER_QUEUE_NAME;
    ProgressBuilder progressBuilder;
    Connection publisherConnection, consumerConnection;
    Channel consumerChannel = null;
    Channel publisherChannel = null;
    AlertDialog dialog = null;
    boolean isDialogShowing;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    RecognitionProgressView recognitionProgressView;
    RelativeLayout parentLayoutChatMsg, rlSend;
    TelephonyManager telephonyManager;
    ConnectivityReceiver connectivityReceiver;
    MyPhoneStateListener phoneStateListener;

    private BroadcastReceiver myRssiChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            int newRssi = arg1.getIntExtra(WifiManager.EXTRA_NEW_RSSI, 0);
            // Toaster.showShortToast(ChatMessageActivity.this, " Rssi Value " + newRssi);
            Log.i("Rssi ChangeReceiver", " Rssi Value " + newRssi);
        }
    };

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connected to Internet";
            color = Color.GREEN;
        } else {
            message = "Sorry! Not connected to internet";
            Toaster.showShortToast(ChatMessageActivity.this,message);
            color = Color.RED;
        }
        Log.i("onNetwork Conn Changed", ": " + message);
//        Snackbar snackbar = Snackbar.make(parentLayoutChatMsg, message, Snackbar.LENGTH_LONG);
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(color);
//        snackbar.show();
    }

    /**
     * PhoneStateListener override method for signal Strength mobile network
     *
     * @param strength
     */
    @Override
    public void signalStrength(int strength) {
        Log.i("Sng Strength ", "Value " + strength);
       if(Connectivity.isConnected(ChatMessageActivity.this) && !Connectivity.isConnectedFast(ChatMessageActivity.this))
        Toaster.showShortToast(ChatMessageActivity.this,"Connection is poor ");

        /**
         * for mobile network 0-30,99
         *
         */
//        if (strength > 0) {
//            if (strength > 70 & strength <= 99) {
//                Log.i("Signal Strength ", "is Excellent   ");
//            } else if (strength > 60 & strength <= 70) {
//                Log.i("Signal Strength ", "is good  ");
//            } else if (strength > 30 & strength <= 60) {
//                Log.i("Signal Strength ", "is Avg  ");
//            } else if (strength > 10 & strength <= 30) {
//                Log.i("Signal Strength ", "is Poor  ");
//            }
//        } else {
//            //for wifi bigger -ve value poor connection
//            if (strength > -53) {
//                Log.i("Signal Strength ", "is good ");
//            } else if (strength < -55) {
//                Log.i("Signal Strength ", "is poor ");
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        /**
         * Checking Enlarge Image layout visible for Image Type
         */
        if (layoutPopIV.getVisibility() == View.VISIBLE) {
            layoutPopIV.setVisibility(View.GONE);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onPause() {
        if (speech != null) {
            speech.destroy();

            Log.i(TAG, "destroy");
        }
        speech = null;
        recognitionProgressView.setVisibility(View.GONE);
        ivMic.setVisibility(View.VISIBLE);
        //Unregister BroadCast Receiver listener
        unregisterReceiver(myRssiChangeReceiver);
        unregisterReceiver(connectivityReceiver);

        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        phoneStateListener=null;
        telephonyManager=null;

        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        if (null == speech)
            getSpeechRecognizer();
        // register BroadCast Receiver listener

        phoneStateListener = new MyPhoneStateListener(this);
        connectivityReceiver = new ConnectivityReceiver(this);
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        this.registerReceiver(this.connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        this.registerReceiver(this.myRssiChangeReceiver, new IntentFilter(WifiManager.RSSI_CHANGED_ACTION));

        super.onResume();


    }
    @Override
    protected void onDestroy() {
/**
 * Interrupt/close Publisher & Subscriber thread
 * Closing Rabbit MQ publisher & consumer connection
 * Clearing message queue
 */
        publishThread.interrupt();
        subscribeThread.interrupt();
        try {
            if (null != publisherConnection && publisherConnection.isOpen())
                publisherConnection.close();
            if (null != consumerConnection && consumerConnection.isOpen())
                consumerConnection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        publishThread = null;
        subscribeThread = null;
        factory = null;
        if (!queue.isEmpty())
            queue.clear();

        super.onDestroy();
    }


    @Override
    public int getLayoutResource() {
        return R.layout.activity_chat_message;
    }

    /**
     * Setup RabbitMQ Connection Factory
     */
    private void setupConnectionFactory() {
        String uri = AppConstants.HOST_NAME;
        try {
            //factory.setAutomaticRecoveryEnabled(false);
            factory.setUri(uri);
        } catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e1) {
            Log.e("SetupConnection Error", e1.getMessage());
            e1.printStackTrace();
        }
    }

    /**
     * Subscribe Consumer, Create Channel & Declare Queue with CONSUMER_QUEUE_NAME as userId for the channel.
     * Getting message from consumer sending message to handler(incomingMessageHandler)
     *
     * @param handler
     */
    void subscribe(final Handler handler) {
        subscribeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Log.i("Consumer ", "Start consuming");

                    try {
                        if (null == consumerConnection || !consumerConnection.isOpen()) {
                            consumerConnection = factory.newConnection();
                        }
                        //Connection connection = factory.newConnection();
                        if (null == consumerChannel || !consumerChannel.isOpen())
                            consumerChannel = consumerConnection.createChannel();


                        //CONSUMER_QUEUE_NAME is userId
                        AMQP.Queue.DeclareOk q = consumerChannel.queueDeclare(CONSUMER_QUEUE_NAME, false, false, false, null);
                        /*----------No need to bind for default----------*/
                        QueueingConsumer consumer = new QueueingConsumer(consumerChannel);

                        consumerChannel.basicConsume(q.getQueue(), true, consumer);
                        // Process deliveries
                        while (true) {
                            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                            String message = new String(delivery.getBody());
                            Log.i("Consumer msg ", "length : " + message.length());
                            if (message.length() > 0) {
                                Log.d("received message", "[r] " + message);
                                Message msg = handler.obtainMessage();
                                Bundle bundle = new Bundle();
                                bundle.putString(AppConstants.MQ_MESSAGE_EXTRA, message);
                                msg.setData(bundle);
                                handler.sendMessage(msg);

                            }
                        }
                    } catch (InterruptedException e) {
                        Log.d("subscrib IntruptExcept", ":" + e.getMessage());

                        break;
                    } catch (Exception e1) {
                        Log.d("", "Connection broken: " + e1.getClass().getName());
                        try {
                            Thread.sleep(4000); //sleep and then try again
                        } catch (InterruptedException e) {
                            Log.d("subscribe Exception", ":" + e.getMessage());
                            break;
                        }
                    }
                }
            }
        });
        subscribeThread.start();
    }

    /**
     * Creating publisherConnection, Channel & publishing message through channel with routingKey(bot id)
     */
    public void publishToAMQP() {
        publishThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {
                    try {
                        if (null == publisherConnection || !publisherConnection.isOpen()) {
                            publisherConnection = factory.newConnection();
                            //Connection connection = factory.newConnection();
                        }

                        if (null == publisherChannel || !publisherChannel.isOpen())
                            publisherChannel = publisherConnection.createChannel();

                        // ch.confirmSelect();
                        while (true) {
                            Log.i("publishToAMQP ", "Start");
                            String message = queue.takeFirst();
                            try {
                                /*----------default exchange empty string ----------*/
                                // ch.basicPublish("", AppConstants.PUBLISHER_QUEUE_NAME, null, message.getBytes());

                                Log.d("PUBLISHER_QUEUE_NAME", ": " + "bot_" + selectedBotId);
                                publisherChannel.basicPublish("", AppConstants.PUBLISHER_QUEUE_NAME + selectedBotId, null, message.getBytes());
                                Log.d("published msg", "msg: [s] " + message);
                            } catch (Exception e) {
                                Log.d("queue msg Exception", "msg exception [f] " + message);
                                queue.putFirst(message);
                                throw e;
                            }
                        }
                    } catch (InterruptedException e) {
                        Log.d("publish InterruptedExpt", ":" + e.getMessage());
                        break;
                    } catch (Exception e) {
                        Log.d("", "Connection broken: " + e.getClass().getName());
                        try {
                            Thread.sleep(3000); //sleep and then try again
                        } catch (InterruptedException e1) {
                            Log.d("publishThread Exception", ":" + e.getMessage());
                            break;
                        }
                    }
                }
            }
        });
        publishThread.start();
    }

    /**
     * Publishing the Message from user edit text message
     *
     * @param et
     */
    void setupPubButton(EditText et) {

        if (!TextUtils.isEmpty(etMessage.getText().toString())) {
            sendTextMsg(etMessage.getText().toString());
        }
        et.setText("");

    }

    private void sendTextMsg(String sendMsg) {
        if (!CommonFunctions.isOnline(ChatMessageActivity.this)) {
            Toaster.showShortToast(ChatMessageActivity.this, getString(R.string.internet_connection_not_available));
            return;
        }

        TextMessage message = new TextMessage(ApiConstants.TEXT_CHAT, sendMsg);
        String json = getGenericPublishMessage(message);
        Log.i("Publish json ", ": " + json);
        publishMessage(json);
        // add your message in list
        list.add(new MessageData(RanderType.TEXT_TYPE, sendMsg, "me", CommonFunctions.getCurrentTime(), 0, "", botName));
        Log.i("PublishButton ", "list count" + list.size());
        adapter.notifyDataSetChanged();
        mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        etMessage.setText("");
    }


    /**
     * Publishing message adding into internal blocking queue
     * so that publishThread consume the queue publish to Rabbit mq
     */
    void publishMessage(String message) {

        dots_progress.setVisibility(View.VISIBLE);
        /**
         * Adds a message to internal blocking queue
         * */
        try {
            Log.d("publishMessage ", "[q] " + message);
            queue.putLast(message);
        } catch (InterruptedException e) {
            dots_progress.setVisibility(View.INVISIBLE);
            Toaster.showShortToast(ChatMessageActivity.this, e.getMessage());
            Log.d("publishMsg Exception", ":" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Log.i("buttonRes", "" + TestData.buttonRes);
        list = new ArrayList<MessageData>();
        initView();
        addListenerToView();
        adapter = new MultiViewTypeAdapter(list, ChatMessageActivity.this, adapterListener, selectedBotId);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);

        getSpeechRecognizer();
        // initializeWiFiListener();


         //     registerNetworkBroadcastForNougat();
    }


    private void addListenerToView() {
        ivSend.setOnClickListener(this);
        ivAddAttachment.setOnClickListener(this);
        ivMic.setOnClickListener(this);
    }

    /**
     * ===============================View Initialization =====================================
     */
    private void initView() {

        userId = BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_ID, "none");
        Log.i("User Id", ": " + userId);

        Intent intent = getIntent();
        if (null != intent && intent.hasExtra(AppConstants.EXTRA_CHAT_BOT_ID)) {
            selectedBotId = intent.getStringExtra(AppConstants.EXTRA_CHAT_BOT_ID);
        }

        parentLayoutChatMsg = (RelativeLayout) findViewById(R.id.parentLayoutChatMsg);
        rlSend = (RelativeLayout) findViewById(R.id.rlSend);
        progressBuilder = new ProgressBuilder(ChatMessageActivity.this);
        dots_progress = (ToolDotProgress) findViewById(R.id.dots_progress);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        ivSend = (ImageView) findViewById(R.id.ivSend);
        ivMic = (ImageView) findViewById(R.id.ivMic);
        ivAddAttachment = (ImageView) findViewById(R.id.ivAddAttachment);
        image_preview = (ImageView) findViewById(R.id.image_preview);
        etMessage = (EditText) findViewById(R.id.etMessage);
        etMessage.addTextChangedListener(this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        layoutPopIV = (RelativeLayout) findViewById(R.id.layoutPopIV);
        ib_close = (ImageView) findViewById(R.id.ib_close);

        setChatWindowTheme();

        ib_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPopIV.setVisibility(View.GONE);
            }
        });

        recognitionProgressView = findViewById(R.id.recognition_view);
        int[] colors = {
                ContextCompat.getColor(this, R.color.color1),
                ContextCompat.getColor(this, R.color.color2),
                ContextCompat.getColor(this, R.color.color3),
                ContextCompat.getColor(this, R.color.color5)
        };

        int[] heights = {16, 20, 14, 19};
        recognitionProgressView.setColors(colors);
        recognitionProgressView.setBarMaxHeightsInDp(heights);
        //   recognitionProgressView.setCircleRadiusInDp(2);
        recognitionProgressView.setSpacingInDp(2);
        recognitionProgressView.setIdleStateAmplitudeInDp(2);
        // recognitionProgressView.setRotationRadiusInDp(10);
        recognitionProgressView.play();


        if (null != intent && intent.hasExtra(AppConstants.EXTRA_CHAT_SCREEN_TITLE)) {
            botName = intent.getStringExtra(AppConstants.EXTRA_CHAT_SCREEN_TITLE);
            toolbar_title.setText(intent.getStringExtra(AppConstants.EXTRA_CHAT_SCREEN_TITLE));
        } else {
            botName = "";
        }


        if (userId.equalsIgnoreCase("none"))
            setDirectChatMsgDepandancy();

        CONSUMER_QUEUE_NAME = userId;
        setupConnectionFactory();
        publishToAMQP();
        setupPubButton(etMessage);
/**
 * Handler for handle incoming message from chat bot
 * */
        final Handler incomingMessageHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                dots_progress.setVisibility(View.GONE);
                String message = msg.getData().getString(AppConstants.MQ_MESSAGE_EXTRA);
                Log.i("incomingMessage", message);

                Gson gson = new Gson();
                ChatMessage chatMsg = gson.fromJson(message, ChatMessage.class);

                /**
                 * Test file type data server json
                 * */
//                Gson gson = new Gson();
//                ChatMessage chatMsg = gson.fromJson(TestData.fileData, ChatMessage.class);

                /**
                 Test button type data server json

                 Gson gson = new Gson();
                 ChatMessage chatMsg = gson.fromJson(TestData.buttonRes, ChatMessage.class);
                 Log.i("TestData.buttonRes", "" + TestData.buttonRes);
                 */
//
//                try {
                for (int i = 0; i < chatMsg.message.size(); i++) {
                    if (chatMsg.message.get(i).type.equalsIgnoreCase(ApiConstants.TEXT_CHAT)) {
                        list.add(new MessageData(RanderType.TEXT_TYPE, chatMsg.message.get(i).text, "bot", CommonFunctions.getCurrentTime(), 0, "", botName));
                        //list.add(new MessageData(RanderType.TEXT_TYPE, "This is our website http://www.google.com you can contact us on 9741542316 or danivishwa@gmail.com we are there for you :)", "bot", CommonFunctions.getCurrentTime(), 0, "",botName));
                    } else if (chatMsg.message.get(i).type.equalsIgnoreCase(ApiConstants.IMAGE_CHAT)) {
                        list.add(new MessageData(RanderType.IMAGE_TYPE, "", "bot", CommonFunctions.getCurrentTime(), 0, chatMsg.message.get(i).image, botName));
                    } else if (chatMsg.message.get(i).type.equalsIgnoreCase(ApiConstants.FILE_CHAT)) {

                        list.add(new MessageData(RanderType.FILE_TYPE, chatMsg.message.get(i).text, "bot", CommonFunctions.getCurrentTime(), chatMsg.message.get(i).filetype, chatMsg.message.get(i).filelink, botName));


                    } else if (chatMsg.message.get(i).type.equalsIgnoreCase(ApiConstants.BUTTON)) {

                        String msg1;

                        buttonArraySize = 0;
                        buttonStartIndex = 0;
                        for (int i1 = 0; i1 < chatMsg.message.size(); i1++) {
                            msg1 = chatMsg.message.get(i1).text;
                            /**
                             * saving button Array size & button start index position in list
                             * for onclick  remove all button objects
                             */
                            buttonArraySize = chatMsg.message.get(i1).button.size();
                            Log.i("button", "size: " + buttonArraySize);
                            for (int j = 0; j < chatMsg.message.get(i1).button.size(); j++) {
                                if (j == 0) {
                                    buttonStartIndex = list.size();
                                    Log.i("button", "buttonStartIndex: " + buttonStartIndex);
                                }
                                if (j != 0)
                                    msg1 = "";
                                list.add(new MessageData(RanderType.BUTTON_TYPE, msg1, chatMsg.message.get(i1).button.get(j).text, chatMsg.message.get(i1).button.get(j).callbackAction, chatMsg.message.get(i1).button.get(j).clickMessage, CommonFunctions.getCurrentTime(), "bot", botName));
                            }
                        }
                        CommonFunctions.hideSoftKeyboard(ChatMessageActivity.this);
                    } else if (chatMsg.message.get(i).type.equalsIgnoreCase(ApiConstants.HYBRID_CHAT)) {
                        createHybridView(chatMsg.message);
                        //list.add(new MessageData(RanderType.PAGER_TYPE, chatMsg.message));

                    }
                }

                // add consumer msg in list
                Log.i("list count", ": " + list.size());
                adapter.notifyDataSetChanged();
                if (adapter.getItemCount() > 1)
                    mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                Log.i("incomingMessage", "Updated");
//
//

            }
        };
        subscribe(incomingMessageHandler);

        /**
         * Interface Implementation for RecyclerView Button/image click listener
         * */
        adapterListener = new MultiViewTypeAdapter.MultiViewTypeAdapterListener() {
            @Override
            public void buttonTypeButtonViewOnClick(View v, int position) {
                ButtonCallBackMessage message = new ButtonCallBackMessage(ApiConstants.CALLBACK, list.get(position).mCallbackAction);
                String json = getGenericPublishMessage(message);
                Log.i("Publish json ", ": " + json);

                //Toaster.showShortToast(ChatMessageActivity.this, "MsgType:" + list.get(position).msgType + "\nCallbackAction: " + list.get(position).mCallbackAction+ "\nClickMessage: " + list.get(position).mClickMessage);
                String clickMsg = list.get(position).mClickMessage;
                clearButtonArray();

                list.add(new MessageData(RanderType.TEXT_TYPE, clickMsg, "bot", CommonFunctions.getCurrentTime(), 0, "", botName));
                adapter.notifyDataSetChanged();
                mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);

                publishMessage(json);

            }

            @Override
            public void imageTypeImageViewOnClick(View v, int position) {
                Log.i("Publish json ", ": " + list.get(position).msgType);
                switch (list.get(position).msgType) {
                    case RanderType.IMAGE_TYPE:
                        layoutPopIV.setVisibility(View.VISIBLE);
                        Glide.with(ChatMessageActivity.this)
                                .load(list.get(position).imageLink)
                                .centerCrop()
                                .crossFade()
                                .into(image_preview);
                        break;
                    case RanderType.FILE_TYPE:
                        //  Toaster.showShortToast(ChatMessageActivity.this, "MsgType:" + list.get(position).msgType + "\nFile Link: " + list.get(position).fileLink);
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).fileLink));
                        startActivity(browserIntent);
                        break;

                }
            }

            @Override
            public void textTypeTextViewOnClick(String txt) {
                if (buttonStartIndex > 0 && buttonArraySize > 0) {
                    clearButtonArray();
                }
                //Toaster.showShortToast(ChatMessageActivity.this, "Msg: " + txt);
                sendTextMsg(txt);
            }
        };

        if (null == selectedBotId || selectedBotId.equalsIgnoreCase("3")) {
            TextMessage message = new TextMessage(ApiConstants.TEXT_CHAT, "/start");
            String json = getGenericPublishMessage(message);
            Log.i("Publish json ", ": " + json);
            publishMessage(json);
        }
    }

    private void setChatWindowTheme() {

        Style chatWindowTheme = BotKitApp.getBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME + selectedBotId);

        if (null != chatWindowTheme) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                parentLayoutChatMsg.setBackground(AppThemeUtils.bgGradientDrawable(chatWindowTheme.bg_color.start_color, chatWindowTheme.bg_color.center_color, chatWindowTheme.bg_color.end_color));
            } else {
                parentLayoutChatMsg.setBackgroundDrawable(AppThemeUtils.bgGradientDrawable(chatWindowTheme.bg_color.start_color, chatWindowTheme.bg_color.center_color, chatWindowTheme.bg_color.end_color));
            }
            if (null != chatWindowTheme.title_bar_color && !chatWindowTheme.title_bar_color.equalsIgnoreCase(""))
                toolbar.setBackgroundColor(Color.parseColor(chatWindowTheme.title_bar_color));
            if (null != chatWindowTheme.title_bar_text_color && !chatWindowTheme.title_bar_text_color.equalsIgnoreCase("")) {
                toolbar.setTitleTextColor(Color.parseColor(chatWindowTheme.title_bar_text_color));
                Drawable drawable = toolbar.getNavigationIcon();
                drawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(chatWindowTheme.title_bar_text_color), PorterDuff.Mode.SRC_IN));
                // toolbar.getNavigationIcon().mutate().setColorFilter(Color.parseColor(chatWindowTheme.title_bar_text_color), PorterDuff.Mode.SRC_IN);
            }
            rlSend.setBackgroundColor(Color.parseColor(chatWindowTheme.title_bar_color));
        }

    }

    /**
     * Removing buttons from chat list
     */
    private void clearButtonArray() {
        int buttonEndIndex = buttonArraySize + buttonStartIndex;
        Log.i("buttonStartIndex", ": " + buttonStartIndex);
        Log.i("buttonEndIndex", ": " + buttonEndIndex);
        Log.i("list.size()", ": " + list.size());
        if (list.size() >= buttonStartIndex && list.size() >= buttonEndIndex)
            list.subList(buttonStartIndex, buttonEndIndex).clear();
    }

    /**
     * =========================SpeechRecognizer & RecognizerIntent Initialization=========
     **/
    private void getSpeechRecognizer() {

        speech = SpeechRecognizer.createSpeechRecognizer(ChatMessageActivity.this);
        speech.setRecognitionListener(ChatMessageActivity.this);
//        Log.i("LOG_TAG", "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this));
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en");
        //indicate package
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        //long time recognition
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra("android.speech.extra.DICTATION_MODE", true);

        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);

    }

    private void setDirectChatMsgDepandancy() {

        userId = "5aaa1a8a861dce7919f00aa6";

        toolbar_title.setText("Pre Sales");
        selectedBotId = "3";

    }

    /**
     * ============Create Dynamic paging/horizontal view & show in dialog ============================
     */
    private void createHybridView(ArrayList<TextMessage> object) {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.hybrid_layout, null);
        LinearLayout llContainer = (LinearLayout) alertLayout.findViewById(R.id.llContainer);


        //default colors
        String cardBgColor = "#FFC6D6C3";
        String btnBgColor = "#277C88";
        String btnTextColor = "#ffffff";
        String botTextColor = "#000000";
        String containerBg = "#FFFFFF";

        llContainer.setBackgroundColor(Color.parseColor(containerBg));
        for (int listPosition = 0; listPosition < object.size(); listPosition++) {

            CardView card = new CardView(ChatMessageActivity.this);
            // Set the CardView layoutParams
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            card.setLayoutParams(params);
            // Set CardView corner radius
            card.setRadius(CommonFunctions.convertDpToPx(9));
            // Set cardView content padding
            card.setContentPadding(15, 15, 15, 15);
            // Set a background color for CardView
            card.setCardBackgroundColor(Color.parseColor(cardBgColor));
            // Set the CardView maximum elevation
            card.setMaxCardElevation(CommonFunctions.convertDpToPx(15));
            // Set CardView elevation
            card.setCardElevation(CommonFunctions.convertDpToPx(9));

            LinearLayout linearLayout_907 = new LinearLayout(ChatMessageActivity.this);
            linearLayout_907.setOrientation(LinearLayout.VERTICAL);
            linearLayout_907.setPadding(CommonFunctions.convertDpToPx(5), CommonFunctions.convertDpToPx(5), CommonFunctions.convertDpToPx(5), CommonFunctions.convertDpToPx(5));
            LinearLayout.LayoutParams layout_371 = new LinearLayout.LayoutParams(CommonFunctions.convertDpToPx(300), LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout_907.setBackgroundColor(Color.parseColor(cardBgColor));
            layout_371.setMargins(CommonFunctions.convertDpToPx(10), CommonFunctions.convertDpToPx(10), CommonFunctions.convertDpToPx(10), CommonFunctions.convertDpToPx(10));
            linearLayout_907.setLayoutParams(layout_371);

            ImageView imageView_964 = new ImageView(ChatMessageActivity.this);
            LinearLayout.LayoutParams layout_851 = new LinearLayout.LayoutParams(CommonFunctions.convertDpToPx(250), CommonFunctions.convertDpToPx(250));
            layout_851.gravity = Gravity.CENTER;
            //imageView_964.setImageResource(R.drawable.image_issue);
            Picasso.with(ChatMessageActivity.this).load(object.get(listPosition).image).resize(250, 250).centerCrop().placeholder(R.drawable.image_issue).into(imageView_964);

            imageView_964.setLayoutParams(layout_851);
            linearLayout_907.addView(imageView_964);

            TextView textView_818 = new TextView(ChatMessageActivity.this);
            textView_818.setTextColor(Color.parseColor(botTextColor));
            LinearLayout.LayoutParams layout_272 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textView_818.setLayoutParams(layout_272);
            textView_818.setText(object.get(listPosition).text);
            linearLayout_907.addView(textView_818);

            for (int j = 0; j < object.get(listPosition).button.size(); j++) {

                Button button_444 = new Button(ChatMessageActivity.this);
                button_444.setBackgroundColor(Color.parseColor(btnBgColor));
                LinearLayout.LayoutParams layout_977 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, CommonFunctions.convertDpToPx(50));
                button_444.setLayoutParams(layout_977);
                button_444.setTextColor(Color.parseColor(btnTextColor));
                ButtonCallBackMessage buttonTag = new ButtonCallBackMessage(ApiConstants.CALLBACK, object.get(listPosition).button.get(j).callbackAction, object.get(listPosition).text);
                button_444.setTag(buttonTag);
                layout_977.setMargins(0, 0, 0, CommonFunctions.convertDpToPx(5));
                button_444.setText(object.get(listPosition).button.get(j).text);
                button_444.setOnClickListener(new ButtonClickListener());
                linearLayout_907.addView(button_444);
            }
            card.addView(linearLayout_907);
            llContainer.addView(card);
        }

        createDialog(alertLayout);
        // this is set the view from XML inside AlertDialog

    }

    private void createDialog(View alertLayout) {
        if (!isDialogShowing) {
            alert = new AlertDialog.Builder(ChatMessageActivity.this, R.style.MyAlertDialogStyle);
            alert.setTitle("");
            alert.setView(alertLayout);

            dialog = alert.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            if (!dialog.isShowing()) {
                CommonFunctions.hideSoftKeyboard(ChatMessageActivity.this);
                dialog.show();
                isDialogShowing = true;
            }
        }

    }

    private void closeDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
            isDialogShowing = false;
        }

    }

    /**
     * =================================OnClick Listener ====================
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSend:
                setupPubButton(etMessage);
                break;
            case R.id.ivAddAttachment:
                showTakeImagePopup();
                break;
            case R.id.ivMic:
                if (Build.VERSION.SDK_INT >= 23) {

                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_PERMISSION);
                } else {
                    //Log.i("recognizerIntent",": "+recognizerIntent.)
                    speech.startListening(recognizerIntent);
                }

                //promptSpeechInput();
                break;
        }
    }


    /**
     * ================EditText text change listener=============
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (etMessage.getText().toString().trim().length() > 0) {
            ivSend.setImageResource(R.drawable.ic_chat_send_active);
            //ivMic.setVisibility(View.GONE);
        } else {
            ivSend.setImageResource(R.drawable.ic_chat_send);
            // ivMic.setVisibility(View.VISIBLE);
        }

    }


    public void showTakeImagePopup() {
        if (Build.VERSION.SDK_INT >= 23) {
            final List<String> permissionsList = new ArrayList<>();
            //addPermission(permissionsList, Manifest.permission.READ_SMS);
            addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            // addPermission(permissionsList, Manifest.permission.CAMERA);

            if (!permissionsList.isEmpty()) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_APP_PERMISSIONS);
                return;
            }
        }
        final CharSequence[] items = {getString(R.string.select_image_from_gallery), getString(R.string.open_camera), "Files", getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.select_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        openGallery();
                        break;
                    case 1:
                        openCamera();
                        break;
                    case 2:
                        openFiles();
                        break;
                    case 3:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void openFiles() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        //intent.setType("*/*");
        String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                "text/plain",
                "application/pdf",
                "application/zip"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        startActivityForResult(intent, PICKFILE_RESULT_CODE);

    }


    /**
     * ======================================================Request & Add permission=======================
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_APP_PERMISSIONS:
                // We require all permissions asked to run app, so close app user rejects any permission
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                    showTakeImagePopup();
                }
                break;
            case REQUEST_RECORD_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (null == speech)
                        getSpeechRecognizer();
                    speech.startListening(recognizerIntent);
                } else {
                    Toast.makeText(ChatMessageActivity.this, "Permission Denied!", Toast
                            .LENGTH_SHORT).show();
                }

        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
        }
        return true;
    }

    /**
     * ================open phone Gallery/Camera===========
     */
    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void openCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                _photoFile = CommonFunctions.createImageFile();
                Log.v(TAG, "mCurrentPhotoPath : " + mCurrentPhotoPath);
                Uri mImageUri = Uri.fromFile(_photoFile);

                Log.v(TAG, "mImageUri for camera : " + mImageUri);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            } catch (Exception e) {
                e.printStackTrace();
                _photoFile = null;
            }
            this.startActivityForResult(intent, CAMERA_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "mImageUri Exception for camera : " + e.getMessage());

        }
    }

    /**
     * ================send request for Crop image which capture from Camera================================
     */
    //
    public void sendCameraSuccessCode(String mCurrentPhotoPath2, int takePhoto) {
        Log.d(TAG, "on camera success");
        String mCurrentPhotoPath = mCurrentPhotoPath2;
        Log.v(TAG, "_photoFile=" + _photoFile);
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
        imagesFolder.mkdirs();
        String filePath = imagesFolder.getAbsolutePath();
        Bitmap bitmap = CommonFunctions.loadImage(mCurrentPhotoPath);

        String newPath = filePath + mCurrentPhotoPath;
        Log.d(TAG, "newPath1...." + newPath);
        CommonFunctions.saveImage(bitmap, mCurrentPhotoPath);
        Intent intent;
        intent = new Intent(this, CropImage.class);
        intent.putExtra("image-path", mCurrentPhotoPath);
        intent.putExtra("scale", true);
        intent.putExtra("circleCrop", false);
        intent.putExtra("return-data", false);
        startActivityForResult(intent, CROP_IMAGE_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT:
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    etMessage.setText(result.get(0));
                }
                break;

            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(TAG, "onActivityResult Camera");
                    sendCameraSuccessCode(_photoFile.toString(), CAMERA_REQUEST);
                }
                break;
            case PICK_IMAGE_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    Log.v(TAG, "pick image Uri=" + uri.toString());
                    ContentResolver cR = getContentResolver();
                    String type = cR.getType(uri);
                    Log.v(TAG, "file type=" + type);
                    String imagePath = CommonFunctions.getRealPathFromURI(uri, this);


                    Intent intent;
                    intent = new Intent(this, CropImage.class);
                    intent.putExtra("image-path", imagePath);
                    intent.putExtra("scale", true);
                    intent.putExtra("circleCrop", false);
                    intent.putExtra("return-data", false);
                    startActivityForResult(intent, CROP_IMAGE_REQUEST);
                }
                break;
            case PICKFILE_RESULT_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    Uri uri = data.getData();
                    Log.v(TAG, "pick file Uri=" + uri.toString());
                    ContentResolver cR = getContentResolver();
                    String type = cR.getType(uri);
                    Log.v(TAG, "file type=" + type);
                    String filePath = CommonFunctions.getRealFilePathFromURI(uri, this);
                    Log.v(TAG, "file path=" + filePath);

                    progressBuilder.showProgressDialog();
                    HashMap<Object, Object> object1 = new HashMap<>();
                    object1.put("type", "chat");
                    if (!BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_ID, "none").equalsIgnoreCase("none"))
                        object1.put("userId", BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_ID, "none"));

                    DataManager.getInstance().postRegisterUser(object1, filePath, new ResultListenerNG<UserSignup>() {
                        @Override
                        public void onSuccess(UserSignup response) {
                            progressBuilder.dismissProgressDialog();
                            checkFileUploadeResponse(response);

                        }

                        @Override
                        public void onError(VolleyError error) {
                            Log.e("Error ", ": " + error.getMessage());

                            progressBuilder.dismissProgressDialog();
                        }
                    });

                }
                break;
            case CROP_IMAGE_REQUEST:
                if (data != null) {
                    mCurrentPhotoPath = data.getExtras().getString("imgPath");
                    progressBuilder.showProgressDialog();
                    HashMap<Object, Object> object1 = new HashMap<>();
                    object1.put("type", "chat");
                    if (!BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_ID, "none").equalsIgnoreCase("none"))
                        object1.put("userId", BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_ID, "none"));

                    DataManager.getInstance().postRegisterUser(object1, mCurrentPhotoPath, new ResultListenerNG<UserSignup>() {
                        @Override
                        public void onSuccess(UserSignup response) {
                            progressBuilder.dismissProgressDialog();
                            checkFileUploadeResponse(response);

                        }

                        @Override
                        public void onError(VolleyError error) {
                            Log.e("Error ", ": " + error.getMessage());

                            progressBuilder.dismissProgressDialog();
                        }
                    });

//                ImageUploadMessage message=new ImageUploadMessage(ApiConstants.IMAGE_CHAT,"fileId");
//                GenericChatMessage<ImageUploadMessage> genText=new GenericChatMessage<>(AppConstants.CONSUMER_QUEUE_NAME,message);
//                Gson gson = new Gson();
//                String json = gson.toJson(genText);
//                Log.i("Publish image json ", ": " + json);
////
//                list.add(new MessageData(RanderType.IMAGE_TYPE, "Sample image text", "me", CommonFunctions.getCurrentTime(), R.drawable.snow, mCurrentPhotoPath));
//                adapter.notifyDataSetChanged();
//                mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
//                Log.i("img post ", "list count" + list.size());
                }
                break;
            case FORM_REQUEST_CODE:
                if (data != null) {
                    String result = data.getStringExtra("chatMsg");
                    TextMessage message = new TextMessage(ApiConstants.TEXT_CHAT, result);
                    String json = getGenericPublishMessage(message);
//                GenericChatMessage<TextMessage> genText = new GenericChatMessage<>(CONSUMER_QUEUE_NAME, message);
//                Gson gson = new Gson();
//                String json = gson.toJson(genText);
                    Log.i("Publish json ", ": " + json);

                    list.add(new MessageData(RanderType.TEXT_TYPE, result, "me", CommonFunctions.getCurrentTime(), 0, "", botName));
                    Log.i("PublishButton ", "list count" + list.size());
                    adapter.notifyDataSetChanged();
                    mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                    publishMessage(json);
                }

                break;
        }

    }

    /**
     * ==================Consume File upload Api Response =================================
     */
    private void checkFileUploadeResponse(UserSignup response) {

        switch (Integer.parseInt(response.status)) {
            case 200:
                ImageUploadMessage message = new ImageUploadMessage(ApiConstants.IMAGE_CHAT, response.fileId);
                //getGenericChatMessage()
                String json = getGenericPublishMessage(message);
                Log.i("Publish image json ", ": " + json);
                publishMessage(json);

                list.add(new MessageData(RanderType.IMAGE_TYPE, "", "me", CommonFunctions.getCurrentTime(), R.drawable.default_img, mCurrentPhotoPath, botName));
                adapter.notifyDataSetChanged();
                mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                Log.i("img post ", "list count" + list.size());
                break;
            case 400:
                break;
        }
    }


    /**
     * ==============================================Begin STT=================================
     */

    @Override
    public void onReadyForSpeech(Bundle bundle) {
        Log.i(TAG, "onReadyForSpeech");
        Toaster.showShortToast(ChatMessageActivity.this, "ReadyForSpeech");
        ivMic.setVisibility(View.GONE);
        recognitionProgressView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onBeginningOfSpeech() {
        Log.i(TAG, "onBeginningOfSpeech");
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.i(TAG, "onRmsChanged: " + rmsdB);
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(TAG, "onEndOfSpeech");
        Toaster.showShortToast(ChatMessageActivity.this, "End Of Speech");
        recognitionProgressView.setVisibility(View.GONE);
        ivMic.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(TAG, "onError " + errorMessage);
        recognitionProgressView.setVisibility(View.GONE);
        ivMic.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        ArrayList<String> matches = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "\n";

        String text1 = "";
        if (matches.size() > 0) {
            text1 = "" + matches.get(0);
        }
        Log.i(TAG, "onPartialResults " + text1);
        etMessage.setText(text1);
    }

    @Override
    public void onResults(Bundle results) {

        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "";

        String text1 = "";
        if (matches.size() > 0) {
            text1 = "" + matches.get(0);
        }
        Log.i(TAG, "onResults " + text1);
        etMessage.setText(text1);
        setupPubButton(etMessage);
    }

    @Override
    public void onEvent(int i, Bundle bundle) {
        Log.i(TAG, "onEvent");
    }


    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }


/**
 * ==============================================End STT=================================
 * */
    /**
     * ====Hybrid/Paging view button click Listener
     */
    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            //Toast.makeText(ChatMessageActivity.this,view.getTag().toString(),Toast.LENGTH_SHORT).show();
            ButtonCallBackMessage tagMessage = (ButtonCallBackMessage) view.getTag();
            ButtonCallBackMessage message = new ButtonCallBackMessage(tagMessage.type, tagMessage.callbackAction);
            String json = getGenericPublishMessage(message);
            Log.i("Publish json ", ": " + json);
            publishMessage(json);
            closeDialog();
            String chMsg = "";
            if (((Button) view).getText().toString().equalsIgnoreCase("BOOK"))
                chMsg = "You booked " + tagMessage.cardText;
            else if (((Button) view).getText().toString().equalsIgnoreCase("CANCEL"))
                chMsg = "You canceled " + tagMessage.cardText;

            list.add(new MessageData(RanderType.TEXT_TYPE, chMsg, "me", CommonFunctions.getCurrentTime(), 0, "", botName));
            adapter.notifyDataSetChanged();
            mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        }
    }

    /**
     * Create PublishMessage using GenericChatMessage class
     */
    private String getGenericPublishMessage(Object message) {
        String json = "";
        Gson gson = new Gson();
        if (message instanceof TextMessage) {
            GenericChatMessage<TextMessage> genText = new GenericChatMessage<>(CONSUMER_QUEUE_NAME, (TextMessage) message);
            json = gson.toJson(genText);
        } else if (message instanceof ImageUploadMessage) {
            GenericChatMessage<ImageUploadMessage> genText = new GenericChatMessage<>(CONSUMER_QUEUE_NAME, (ImageUploadMessage) message);
            json = gson.toJson(genText);
        } else if (message instanceof ButtonCallBackMessage) {
            GenericChatMessage<ButtonCallBackMessage> genText = new GenericChatMessage<>(CONSUMER_QUEUE_NAME, (ButtonCallBackMessage) message);
            json = gson.toJson(genText);
        }
        return json;
    }

}
