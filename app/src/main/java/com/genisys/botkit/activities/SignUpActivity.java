package com.genisys.botkit.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.BuildConfig;
import com.genisys.botkit.R;
import com.genisys.botkit.api.pojo.UserSignup;
import com.genisys.botkit.network.ApiServiceInterface;
import com.genisys.botkit.network.AppClient;
import com.genisys.botkit.network.volley.DataManager;
import com.genisys.botkit.network.volley.ResultListenerNG;
import com.genisys.botkit.util.ApiConstants;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.NumericKeyBoardTransformationMethod;
import com.genisys.botkit.util.ProgressBuilder;
import com.genisys.botkit.util.Toaster;
import com.genisys.botkit.util.Validation;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.genisys.botkit.R.id.etPhoneNumber;

public class SignUpActivity extends SignUpBaseClass implements View.OnClickListener {

    private ImageView imageProfile;
    private EditText etFirstName, etLastName;
    private EditText etEmailAddress;
    private TextView tvGender, tvDoB, tvPhoneNumber;
    private Button btnSubmit;
    private ProgressBuilder progressBuilder;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initView();
        addListeners();
    }

    private void addListeners() {
        btnSubmit.setOnClickListener(this);
        tvDoB.setOnClickListener(this);
        tvGender.setOnClickListener(this);
        imageProfile.setOnClickListener(this);
    }

    private void initView() {

        progressBuilder = new ProgressBuilder(SignUpActivity.this);
        imageProfile = (ImageView) findViewById(R.id.ivUserPic);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        tvDoB = (TextView) findViewById(R.id.tvDoB);
        tvPhoneNumber = (TextView) findViewById(etPhoneNumber);
        tvPhoneNumber.setTransformationMethod(new NumericKeyBoardTransformationMethod());

        etEmailAddress = (EditText) findViewById(R.id.etEmailAddress);
        tvGender = (TextView) findViewById(R.id.tvGender);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        tvPhoneNumber.setText(BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.PHONE_NUMBER, ""));
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvDoB.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean validate() {
        if (TextUtils.isEmpty(mCurrentPhotoPath)) {
            Toaster.showShortToast(SignUpActivity.this, getString(R.string.upload_ur_photo));
            return false;
        }
        if (!TextUtils.isEmpty(etFirstName.getText().toString()) && !TextUtils.isEmpty(etLastName.getText().toString()) && !TextUtils.isEmpty(tvDoB.getText().toString()) && !TextUtils.isEmpty(tvGender.getText().toString()) && !TextUtils.isEmpty(tvPhoneNumber.getText().toString()) && !TextUtils.isEmpty(etEmailAddress.getText().toString())) {

            // check email format

            if (!Validation.isEmailAddress(etEmailAddress, true)) {
                Toaster.showShortToast(SignUpActivity.this, getString(R.string.please_enter_valid_email_address));
                return false;
            }
            return true;
        } else {
            Toaster.showShortToast(SignUpActivity.this, getString(R.string.please_enter_mandatory_field));
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (validate()) {
                    registerUser();
                }
                break;
            case R.id.tvDoB:
                showDatePicker();
                break;
            case R.id.tvGender:
                showDialog(SignUpActivity.this, "Select Your Gender");
                break;
            case R.id.ivUserPic:
                showTakeImagePopup(imageProfile);
                break;
        }
    }

    private void showDatePicker() {
        DatePickerDialog datePicker = new DatePickerDialog(SignUpActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.show();

    }

    public void showDialog(Context c, String title) {
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_list_item_1);
        arrayAdapter.add("Male");
        arrayAdapter.add("Female");
        arrayAdapter.add("Neutral");

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle(title);
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvGender.setText(arrayAdapter.getItem(which));
                dialog.dismiss();
            }
        });

        builder.show();


    }

    //Register user api call
    private void registerUser() {

        if (!CommonFunctions.isOnline(SignUpActivity.this)) {
            Toaster.showShortToast(SignUpActivity.this, getString(R.string.internet_connection_not_available));
            return;
        }
        progressBuilder.showProgressDialog();
        ApiServiceInterface apiClient = AppClient.getClient().create(ApiServiceInterface.class);

        JsonObject object = new JsonObject();
        object.addProperty(ApiConstants.FIRST_NAME, etFirstName.getText().toString());
        object.addProperty(ApiConstants.LAST_NAME, etLastName.getText().toString());
        object.addProperty(ApiConstants.PHONE, tvPhoneNumber.getText().toString());
        object.addProperty(ApiConstants.EMAIL, etEmailAddress.getText().toString());
        object.addProperty(ApiConstants.DOB, tvDoB.getText().toString());
        object.addProperty(ApiConstants.GENDER, tvGender.getText().toString());


        apiClient.postRegisterUser(object).enqueue(new Callback<UserSignup>() {
            @Override
            public void onResponse(Call<UserSignup> call, Response<UserSignup> response) {
                progressBuilder.dismissProgressDialog();
                checkResponseBody(response.body());
            }

            @Override
            public void onFailure(Call<UserSignup> call, Throwable t) {
                progressBuilder.dismissProgressDialog();
            }
        });


    }

    //Server Respose for Register api
    private void checkResponseBody(UserSignup body) {
        switch (Integer.valueOf(body.status)) {
            case 200:
                BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.IS_USER_REGISTER, "Yes").commit();
                saveUserDetails(body);

                if (!TextUtils.isEmpty(mCurrentPhotoPath))
                    BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_LOCAL_PHOTO_PATH, mCurrentPhotoPath).commit();

                //Uploading user pic using registered user id
                if (null != body.user_details._id && !TextUtils.isEmpty(mCurrentPhotoPath)) {
                    progressBuilder.showProgressDialog();
                    HashMap<Object, Object> object1 = new HashMap<>();
                    object1.put("type", "profile_pic");

                    if (body.user_details._id.trim().length() > 0)
                        object1.put("userId", body.user_details._id);
                    else
                        object1.put("userId", "none");

                    DataManager.getInstance().postRegisterUser(object1, mCurrentPhotoPath, new ResultListenerNG<UserSignup>() {
                        @Override
                        public void onSuccess(UserSignup response) {
                            progressBuilder.dismissProgressDialog();
                            checkFileUploadeResponse(response);

                        }

                        @Override
                        public void onError(VolleyError error) {
                            Log.e("Error ", ": " + error.getMessage());
                            progressBuilder.dismissProgressDialog();
                            Toaster.showShortToast(SignUpActivity.this, getString(R.string.file_upload_fail) + "\n Error: " + error.getMessage());

                        }
                    });
                } else {

                    startHomeActivity();

                }

                Log.i("User Details ", ": " + body.user_details.toString());
                break;
            default:
                Toaster.showShortToast(SignUpActivity.this, getString(R.string.serverbusy));

        }
    }

    //****Saving registered user detail
    private void saveUserDetails(UserSignup body) {
        if (null != body.user_details._id) {
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_ID, body.user_details._id).commit();

        }
        if (null != body.user_details.first_name) {
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_FIRST_NAME, body.user_details.first_name).commit();

        }
        if (null != body.user_details.last_name) {
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_LAST_NAME, body.user_details.last_name).commit();

        }
        if (null != body.user_details.email) {
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_EMAIL, body.user_details.email).commit();

        }
        if (null != body.fileId) {
            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.PROFILE_PHOTO_URL, BuildConfig.SERVER_BASE_URL + "\\" + body.fileId).commit();
        }
    }

    private void startHomeActivity() {
        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
        intent.putExtra(AppConstants.REGISTRATION_FLOW, "Yes");
        startActivity(intent);
//      startActivity(new Intent(SignUpActivity.this, SelectedBotActivity.class));
        finish();
    }

    // Upload pic api server response for user pic
    private void checkFileUploadeResponse(UserSignup response) {
        switch (Integer.parseInt(response.status)) {
            case 200:
                if (null != response.fileId) {
                    BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.PROFILE_PIC_FILE_ID, response.fileId).commit();
                    Log.i("upload ", "file id : " + response.fileId);
                }
                startHomeActivity();
                break;
            case 400:
                Toaster.showShortToast(SignUpActivity.this, "Profile pic upload 400 error");
                break;
        }
    }

    @Override
    protected void onImageSet() {

    }
}
