package com.genisys.botkit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.api.pojo.UserSignup;
import com.genisys.botkit.fragments.BotListFragment;
import com.genisys.botkit.fragments.MyBotsFragment;
import com.genisys.botkit.network.volley.DataManager;
import com.genisys.botkit.network.volley.ResultListenerNG;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.LoginHandler;

import java.util.HashMap;

public class HomeActivity extends HomeBaseActivity implements View.OnClickListener {
    private DrawerLayout mDrawerLayout;
    private ImageView navigationButton;
    private ScrollView svForNavigationDrawer;
    private TextView tvName, tvEmail;
    private LinearLayout llMyProfile, llMyBots, llAbout, llSetting, llHelp, llLogOut;
private boolean logout=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initView();
        addListeners();
        setNameAndImage();
        initializeFirstFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if(!logout)
//        android.os.Process.killProcess(android.os.Process.myPid());
    }

    // ========================Set Name And Image on Side Menu===============
    public void setNameAndImage() {

        String name = BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_FIRST_NAME, "");
        tvName.setText(TextUtils.isEmpty(name) ? "" : name);
        String email = BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.USER_EMAIL, "");
        tvEmail.setText(TextUtils.isEmpty(email) ? "" : email);



//        // Set photo
//        String photoUrl = BotKitApp.sharedPreferences.getString(ApplicationConstants.PREFERENCES.NEW_PHOTO_URL, "");
//        if (TextUtils.isEmpty(photoUrl)) {
//            photoUrl = BotKitApp.sharedPreferences.getString(ApplicationConstants.PHOTO_URL, "");
//        }
//        if (!TextUtils.isEmpty(photoUrl)) {
//            Picasso.with(this).load(photoUrl).resize(100, 100)
//                    .centerCrop()
//                    .transform(new CircleTransform())
//                    .placeholder(R.drawable.addphto).into(imgUser);
//        }
    }

    private void addListeners() {

        navigationButton.setOnClickListener(this);
        svForNavigationDrawer.setOnClickListener(this);
        llMyProfile.setOnClickListener(this);
        llMyBots.setOnClickListener(this);
        llAbout.setOnClickListener(this);
        llSetting.setOnClickListener(this);
        llHelp.setOnClickListener(this);
        llLogOut.setOnClickListener(this);

    }

    private void initView() {
        navigationButton = (ImageView) findViewById(R.id.navigationButton);
        svForNavigationDrawer = (ScrollView) findViewById(R.id.svForNavigationDrawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);
        llMyProfile = (LinearLayout) findViewById(R.id.llMyProfile);
        llMyBots = (LinearLayout) findViewById(R.id.llMyBots);
        llAbout = (LinearLayout) findViewById(R.id.llAbout);
        llSetting = (LinearLayout) findViewById(R.id.llSetting);
        llHelp = (LinearLayout) findViewById(R.id.llHelp);
        llLogOut = (LinearLayout) findViewById(R.id.llLogOut);

        tvName = (TextView) findViewById(R.id.tvName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
    }

//    public void setMenuIcon(){
//        navigationButton.setVisibility(View.VISIBLE);
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//    }
    private void initializeFirstFragment() {
        System.gc();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Intent intent = getIntent();
        if (intent.hasExtra(AppConstants.REGISTRATION_FLOW)) {
//            navigationButton.setVisibility(View.GONE);
//            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            fragmentTransaction.replace(R.id.content_frame, new BotListFragment());

        } else {
               fragmentTransaction.replace(R.id.content_frame, new MyBotsFragment());
        }

       // fragmentTransaction.replace(R.id.content_frame, new BotListFragment());

        fragmentTransaction.commit();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navigationButton:
                if (mDrawerLayout.isDrawerOpen(svForNavigationDrawer)) {
                    mDrawerLayout.closeDrawer(svForNavigationDrawer);
                } else {
                    mDrawerLayout.openDrawer(svForNavigationDrawer);
                }
                break;
            case R.id.llMyProfile:
                break;
            case R.id.llMyBots:
                System.gc();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, new MyBotsFragment());
                fragmentTransaction.commit();
                mDrawerLayout.closeDrawer(svForNavigationDrawer);
                break;
            case R.id.llAbout:
                break;
            case R.id.llSetting:
                break;
            case R.id.llHelp:
                break;
            case R.id.llLogOut:
                logout=true;
                LoginHandler.logout(HomeActivity.this);
                mDrawerLayout.closeDrawer(svForNavigationDrawer);
                break;
        }
    }
}
