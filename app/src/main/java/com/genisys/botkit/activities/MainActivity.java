package com.genisys.botkit.activities;

import android.app.Activity;

//import com.rabbitmq.client.QueueingConsumer;

public class MainActivity extends Activity {
//
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
//		setupConnectionFactory();
//		publishToAMQP();
//		setupPubButton();
//
//		final Handler incomingMessageHandler = new Handler() {
//			@Override
//			public void handleMessage(Message msg) {
//				Log.i("Message",msg.getData().toString());
//				String message = msg.getData().getString(AppConstants.MQ_MESSAGE_EXTRA);
//				TextView tv = (TextView) findViewById(R.id.textView);
//				Date now = new Date();
//				SimpleDateFormat ft = new SimpleDateFormat ("hh:mm:ss");
//				tv.append(ft.format(now) + ' ' + message + '\n');
//			}
//		};
//		subscribe(incomingMessageHandler);
//	}
//
//	void setupPubButton() {
//		Button button = (Button) findViewById(R.id.publish);
//		button.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				EditText et = (EditText) findViewById(R.id.text);
//				publishMessage(et.getText().toString());
//				et.setText("");
//			}
//		});
//	}
//
//	Thread subscribeThread;
//	Thread publishThread;
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		publishThread.interrupt();
//		subscribeThread.interrupt();
//	}
//
//	private BlockingDeque<String> queue = new LinkedBlockingDeque<String>();
//	void publishMessage(String message) {
//		//Adds a message to internal blocking queue
//		try {
//			Log.d("publishMessage ","[q] " + message);
//			queue.putLast(message);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}
//
//	ConnectionFactory factory = new ConnectionFactory();
//	private void setupConnectionFactory() {
//		String uri = AppConstants.HOST_NAME;
//		try {
//			//factory.setAutomaticRecoveryEnabled(false);
//			factory.setUri(uri);
//
//		} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e1) {
//			e1.printStackTrace();
//		}
//	}
//
//	void subscribe(final Handler handler)
//	{
//		subscribeThread = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				while(true) {
//					Log.i("subscribe ","Loop");
//					try {
//						Connection connection = factory.newConnection();
//						Channel channel = connection.createChannel();
//						channel.basicQos(1);
//						//DeclareOk q = channel.queueDeclare();
//						DeclareOk q = channel.queueDeclare(AppConstants.CONSUMER_QUEUE_NAME, false, false, false, null);
//
//						//channel.queueBind(q.getQueue(), "amq.fanout", AppConstants.QUEUE_NAME);
//						//channel.queueBind(q.getQueue(), "amq.fanout", AppConstants.CHANNEL_NAME);
//
//						/*----------No need to bind for default----------*/
//						QueueingConsumer consumer = new QueueingConsumer(channel);
//						channel.basicConsume(q.getQueue(), true, consumer);
//
//						// Process deliveries
//						while (true) {
//							QueueingConsumer.Delivery delivery = consumer.nextDelivery();
//							String message = new String(delivery.getBody());
//							Log.d("","[r] " + message);
//							Message msg = handler.obtainMessage();
//							Bundle bundle = new Bundle();
//							bundle.putString(AppConstants.MQ_MESSAGE_EXTRA, message);
//							msg.setData(bundle);
//							handler.sendMessage(msg);
//						}
//					} catch (InterruptedException e) {
//						break;
//					} catch (Exception e1) {
//						Log.d("", "Connection broken: " + e1.getClass().getName());
//						try {
//							Thread.sleep(4000); //sleep and then try again
//						} catch (InterruptedException e) {
//							break;
//						}
//					}
//				}
//			}
//		});
//		subscribeThread.start();
//	}
//
//	public void publishToAMQP()
//	{
//		publishThread = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				while(true) {
//					try {
//						Connection connection = factory.newConnection();
//						Channel ch = connection.createChannel();
//						ch.confirmSelect();
//
//						while (true) {
//							Log.i("publishToAMQP ","Loop");
//							String message = queue.takeFirst();
//							try{
//								//ch.exchangeDeclare(EXCHANGE_NAME, "fanout", true);
//								//ch.queueDeclare(AppConstants.QUEUE_NAME, false, false, false, null);
//								//ch.basicPublish("amq.fanout", AppConstants.QUEUE_NAME, null, message.getBytes());
//								/*----------default exchange empty string ----------*/
//								ch.basicPublish("", AppConstants.PUBLISHER_QUEUE_NAME, null, message.getBytes());
//								Log.d("queue msg", "[s] " + message);
//								ch.waitForConfirmsOrDie();
//							} catch (Exception e){
//								Log.d("queue msg Exception","[f] " + message);
//								queue.putFirst(message);
//								throw e;
//							}
//						}
//					} catch (InterruptedException e) {
//						Log.d("InterruptedException",":"+e.getMessage());
//						break;
//					} catch (Exception e) {
//						Log.d("", "Connection broken: " + e.getClass().getName());
//						try {
//							Thread.sleep(5000); //sleep and then try again
//						} catch (InterruptedException e1) {
//							break;
//						}
//					}
//				}
//			}
//		});
//		publishThread.start();
//	}
}
