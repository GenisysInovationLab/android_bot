package com.genisys.botkit.activities;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.genisys.botkit.TestData;
import com.genisys.botkit.api.pojo.FormGui;
import com.genisys.botkit.util.Toaster;
import com.genisys.botkit.widgets.GuiDate;
import com.genisys.botkit.widgets.GuiEditBox;
import com.genisys.botkit.widgets.GuiPickMany;
import com.genisys.botkit.widgets.GuiPickOne;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DynamicForm extends AppCompatActivity {

    FormGui form;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Bundle d=getIntent().getExtras();
//        String formResp=d.getString("formResp");

        Gson gson = new Gson();
        //form = gson.fromJson(formResp, FormGui.class);
        form = gson.fromJson(TestData.formResp, FormGui.class);
        Log.i("form", "form data " + form);
        Log.i("form", "form field size " + form.form.field.size());
        for (int i = 0; i < form.form.field.size(); i++) {
            Log.i("type", "form field size " + form.form.field.get(i).type);
        }
        displayUi();
        myCalendar = Calendar.getInstance();
    }

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            ((GuiDate) v).setValue(sdf.format(myCalendar.getTime()));
        }

    };
    View v;

    private void displayUi() {
        ScrollView sv = new ScrollView(this);
        Log.i("bgcolor", form.form.theme.bg_color);
        sv.setBackgroundColor(Color.parseColor(form.form.theme.bg_color));
        final LinearLayout ll = new LinearLayout(this);
        ll.setPadding(20, 20, 20, 20);
        sv.addView(ll);
        ll.setOrientation(android.widget.LinearLayout.VERTICAL);
        TextView title = new TextView(DynamicForm.this);
        title.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        title.setText(form.form.title.label);
        title.setTextColor(Color.parseColor(form.form.theme.title_text_font_color));
        title.setTextSize(Integer.parseInt(form.form.title.fontSize));
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 20, 0, 20);
        ll.addView(title);

        for (int i = 0; i < form.form.field.size(); i++) {
            if (form.form.field.get(i).type.equalsIgnoreCase("TextBox")) {
                form.form.field.get(i).obj = new GuiEditBox(this, form.form.field.get(i).label + (form.form.field.get(i).required.equalsIgnoreCase("Y") ? "*" : ""), "", form.form.theme);

                //  ((GuiEditBox) form.form.field.get(i).obj).setEditTextBackground(R.drawable.edit_text_bg);
                ll.addView((View) form.form.field.get(i).obj);
            }
            if (form.form.field.get(i).type.equalsIgnoreCase("numeric")) {
                // form.form.field.get(i).obj = new GuiEditBox(this, form.form.field.get(i).label + (form.form.field.get(i).required.equalsIgnoreCase("Y") ? "*" : ""), "");
                form.form.field.get(i).obj = new GuiEditBox(this, form.form.field.get(i).label + (form.form.field.get(i).required.equalsIgnoreCase("Y") ? "*" : ""), "", form.form.theme);

                ((GuiEditBox) form.form.field.get(i).obj).makeNumeric();
                // ((GuiEditBox) form.form.field.get(i).obj).setEditTextBackground(R.drawable.edit_text_bg);
                ll.addView((View) form.form.field.get(i).obj);
            }
            if (form.form.field.get(i).type.equalsIgnoreCase("Choice")) {
                form.form.field.get(i).obj = new GuiPickOne(this, form.form.field.get(i).label + (form.form.field.get(i).required.equalsIgnoreCase("Y") ? "*" : ""), form.form.field.get(i).options, form.form.theme);
                ll.addView((View) form.form.field.get(i).obj);
            }
            if (form.form.field.get(i).type.equalsIgnoreCase("multichoice")) {

//                for(int j=0;j<form.form.field.get(i).options.size();j++){
//
                form.form.field.get(i).obj = new GuiPickMany(this, form.form.field.get(i).label + (form.form.field.get(i).required.equalsIgnoreCase("Y") ? "*" : ""), form.form.field.get(i).options, form.form.theme);
//                }
                ll.addView((View) form.form.field.get(i).obj);
            }
            if (form.form.field.get(i).type.equalsIgnoreCase("date")) {
                form.form.field.get(i).obj = new GuiDate(this, form.form.field.get(i).label + (form.form.field.get(i).required.equalsIgnoreCase("Y") ? "*" : ""), "dd/mm/yyyy", form.form.theme);
                ((GuiDate) form.form.field.get(i).obj).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DatePickerDialog(DynamicForm.this, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                        v = ((GuiDate) view);
                        // ((GuiButton) view).setValue("17/01/2018");
                    }
                });

                //        ((GuiDate) form.form.field.get(i).obj).setButtonBackground(R.drawable.rounded_edittext);
                ll.addView((View) form.form.field.get(i).obj);
            }


        }
//        form.setFields((Vector<Field>) form.form.field);
        Button btn = new Button(this);
        btn.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        btn.setBackgroundColor(Color.parseColor(form.form.theme.button_color));
        btn.setTextColor(Color.parseColor(form.form.theme.button_text_color));
        ll.addView(btn);
        btn.setText("Submit");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean good;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < form.form.field.size(); i++) {
                    if (form.form.field.get(i).type.equalsIgnoreCase("TextBox")) {
                        if (form.form.field.get(i).obj != null) {
                            GuiEditBox b = (GuiEditBox) form.form.field.get(i).obj;
                            if (form.form.field.get(i).required.equalsIgnoreCase("Y") && b.getValue().trim().length() < 0) {
                                Toaster.showShortToast(DynamicForm.this, "Enter Required fields ");
                                return;
                            }

                            b.getValue();
                            Log.i("Tag ", form.form.field.get(i).getName() + " is [" + b.getValue() + "]");
                            sb.append(form.form.field.get(i).getName() + " is " + b.getValue() + "");
                        }
                    }

                    if (form.form.field.get(i).type.equalsIgnoreCase("Choice")) {
                        if (form.form.field.get(i).obj != null) {
                            GuiPickOne b = (GuiPickOne) form.form.field.get(i).obj;
                            if (form.form.field.get(i).required.equalsIgnoreCase("Y") && b.getValue().trim().length() < 0) {
                                Toaster.showShortToast(DynamicForm.this, "Enter Required fields ");
                                return;
                            }

                            Log.i("Tag ", form.form.field.get(i).getName() + " is [" + b.getValue() + "]");
                            sb.append(form.form.field.get(i).getName() + " is " + b.getValue() + "");

                        }

                    }

                    if (form.form.field.get(i).type.equalsIgnoreCase("multichoice")) {
                        if (form.form.field.get(i).obj != null) {
                            GuiPickMany b = (GuiPickMany) form.form.field.get(i).obj;
                            if (form.form.field.get(i).required.equalsIgnoreCase("Y") && b.getValue().size() < 0) {
                                Toaster.showShortToast(DynamicForm.this, "Enter Required fields ");
                                return;
                            }

                            Log.i("Tag ", form.form.field.get(i).getName() + " is [" + b.getValue() + "]");
                            sb.append(form.form.field.get(i).getName() + " is " + b.getValue() + "");

                        }

                    }
                    if (form.form.field.get(i).type.equalsIgnoreCase("numeric")) {
                        if (form.form.field.get(i).obj != null) {
                            GuiEditBox b = (GuiEditBox) form.form.field.get(i).obj;
                            if (form.form.field.get(i).required.equalsIgnoreCase("Y") && b.getValue().trim().length() < 0) {
                                Toaster.showShortToast(DynamicForm.this, "Enter Required fields ");
                                return;
                            }
                            Log.i("Tag ", form.form.field.get(i).getName() + " is [" + b.getValue() + "]");
                            sb.append(form.form.field.get(i).getName() + " is " + b.getValue() + "");
                        }
                    }
                    if (form.form.field.get(i).type.equalsIgnoreCase("date")) {
                        if (form.form.field.get(i).obj != null) {
                            GuiDate b = (GuiDate) form.form.field.get(i).obj;
                            if (form.form.field.get(i).required.equalsIgnoreCase("Y") && b.getValue().trim().length() < 0) {
                                Toaster.showShortToast(DynamicForm.this, "Enter Required fields ");
                                return;
                            }
                            Log.i("Tag ", form.form.field.get(i).getName() + " is [" + b.getValue() + "]");
                            sb.append(form.form.field.get(i).getName() + " is " + b.getValue() + "");
                        }
                    }
                    sb.append(System.getProperty("line.separator"));
                }
                Log.i("form data ", ": " + sb.toString());
//                Intent intent = new Intent();
//                intent.putExtra("chatMsg",sb.toString());
//                setResult(Activity.RESULT_OK, intent);
//                finish();
//                Toaster.showShortToast(DynamicForm.this, sb.toString());
            }
        });

        setContentView(sv);
    }
}
