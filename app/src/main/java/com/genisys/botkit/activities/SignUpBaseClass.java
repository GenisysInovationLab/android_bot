package com.genisys.botkit.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ImageView;

import com.genisys.botkit.R;
import com.genisys.botkit.cropImage.CropImage;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.RoundedImage;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by vishwanathd on 08-12-2017.
 */

public abstract class SignUpBaseClass extends Activity {
    private static final String TAG = SignUpBaseClass.class.getSimpleName();
    private static final int REQUEST_CODE_APP_PERMISSIONS = 1;
    public String mCurrentPhotoPath = "";
    public static File _photoFile;

    public static final int CAMERA_REQUEST = 0;
    public static final int PICK_IMAGE_REQUEST = 1;
    public static final int CROP_IMAGE_REQUEST = 3;
    private ImageView mImageProfile;

    public String isoCountryCode;
    protected String address;
    protected double lng, lat;

    protected abstract void onImageSet();

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_APP_PERMISSIONS: {
                // We require all permissions asked to run app, so close app user rejFects any permission
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                    showTakeImagePopup(mImageProfile);
                }
            }
        }
    }

    // ==========================Cropping functionality====================
    public void showTakeImagePopup(ImageView imageProfile) {
        mImageProfile = imageProfile;
        if (Build.VERSION.SDK_INT >= 23) {
            final List<String> permissionsList = new ArrayList<>();
            //addPermission(permissionsList, Manifest.permission.READ_SMS);
            addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE);
           // addPermission(permissionsList, Manifest.permission.CAMERA);

            if (!permissionsList.isEmpty()) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_APP_PERMISSIONS);
                return;
            }
        }
        final CharSequence[] items = {getString(R.string.select_image_from_gallery), getString(R.string.open_camera), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.select_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        openGallery();
                        break;
                    case 1:
                        openCamera();
                        break;
                    case 3:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "onActivityResult Camera");
            sendCameraSuccessCode(_photoFile.toString(), CAMERA_REQUEST);
        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            Log.v(TAG, "pick image Uri=" + uri);
            ContentResolver cR = getContentResolver();
            String type = cR.getType(uri);
            Log.v(TAG, "file type=" + type);
            String imagePath = CommonFunctions.getRealPathFromURI(uri, this);
            Intent intent;
            intent = new Intent(this, CropImage.class);
            intent.putExtra("image-path", imagePath);
            intent.putExtra("scale", true);
            intent.putExtra("circleCrop", false);
            intent.putExtra("return-data", false);
            startActivityForResult(intent, CROP_IMAGE_REQUEST);
        } else if (requestCode == CROP_IMAGE_REQUEST) {
            if (data != null) {
                mCurrentPhotoPath = data.getExtras().getString("imgPath");
                Log.i("ProfilePic ","path "+mCurrentPhotoPath);
                setImageFromLocal(mImageProfile);
                onImageSet();
            }
        }
    }
/**
* create rounded Image and set as profile pic
 */
    public void setImageFromLocal(ImageView imageProfile)
    {
        Bitmap bitmap = CommonFunctions.decodeFile(mCurrentPhotoPath);
        bitmap = RoundedImage.getRoundedShape(bitmap);
        imageProfile.setImageBitmap(CommonFunctions.createScaledBitmap(bitmap));
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void openCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                _photoFile = CommonFunctions.createImageFile();
                Log.v(TAG, "mCurrentPhotoPath : " + mCurrentPhotoPath);
                Uri mImageUri = Uri.fromFile(_photoFile);

                Log.v(TAG, "mImageUri for camera : " + mImageUri);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            } catch (Exception e) {
                e.printStackTrace();

                  _photoFile = null;
            }
            this.startActivityForResult(intent, CAMERA_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "mImageUri Exception for camera : " + e.getMessage());

        }
    }

    public void sendCameraSuccessCode(String mCurrentPhotoPath2, int takePhoto) {
        Log.d(TAG, "on camera success");
        String mCurrentPhotoPath = mCurrentPhotoPath2;
        Log.v(TAG, "_photoFile=" + _photoFile);
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
        imagesFolder.mkdirs();
        String filePath = imagesFolder.getAbsolutePath();
        Bitmap bitmap = CommonFunctions.loadImage(mCurrentPhotoPath);

        String newPath = filePath + mCurrentPhotoPath;
        Log.d(TAG, "newPath1...." + newPath);
        CommonFunctions.saveImage(bitmap, mCurrentPhotoPath);
        Intent intent;
        intent = new Intent(this, CropImage.class);
        intent.putExtra("image-path", mCurrentPhotoPath);
        intent.putExtra("scale", true);
        intent.putExtra("circleCrop", false);
        intent.putExtra("return-data", false);
        startActivityForResult(intent, CROP_IMAGE_REQUEST);
    }

    protected void showDialogCountry(String mCountry) {
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<>();
        String country;
        for (Locale loc : locale) {
            country = loc.getDisplayCountry();
            if (mCountry.trim().equalsIgnoreCase(country)) {
                isoCountryCode = loc.getCountry();
                break;
            }
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);
    }



}
