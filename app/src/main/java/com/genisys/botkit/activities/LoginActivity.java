package com.genisys.botkit.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.R;
import com.genisys.botkit.api.pojo.OtpVerify;
import com.genisys.botkit.api.pojo.UserDetails;
import com.genisys.botkit.network.ApiServiceInterface;
import com.genisys.botkit.network.AppClient;
import com.genisys.botkit.util.ApiConstants;
import com.genisys.botkit.util.AppConstants;
import com.genisys.botkit.util.CommonFunctions;
import com.genisys.botkit.util.NumericKeyBoardTransformationMethod;
import com.genisys.botkit.util.ProgressBuilder;
import com.genisys.botkit.util.Toaster;
import com.genisys.botkit.util.Validation;
import com.google.gson.JsonObject;
import com.sithagi.countrycodepicker.Country;
import com.sithagi.countrycodepicker.CountryPicker;
import com.sithagi.countrycodepicker.CountryPickerListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends FragmentActivity implements View.OnClickListener {

    private LinearLayout linearLayoutOtp;
    private Button btnEnterStart;
    private TextView btnResend, tvCountryPhCode;
    private EditText etPhone, etOtp1, etOtp2, etOtp3, etOtp4;
    private ProgressBuilder progressBuilder;
    public String isoCountryCode;
    CountryPicker picker;
    private String TAG = LoginActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        addListenerToView();
        picker = CountryPicker.newInstance("SelectCountry");
        showDialogCountry("IN");

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                tvCountryPhCode.setText(dialCode);
                picker.dismiss();
            }
        });


    }

    //setOnClickListener to the View
    private void addListenerToView() {
        btnEnterStart.setOnClickListener(this);
        btnResend.setOnClickListener(this);

        etOtp1.addTextChangedListener(new GenericTextWatcher(etOtp1));
        etOtp2.addTextChangedListener(new GenericTextWatcher(etOtp2));
        etOtp3.addTextChangedListener(new GenericTextWatcher(etOtp3));
        etOtp4.addTextChangedListener(new GenericTextWatcher(etOtp4));
    }

    private void initView() {

        progressBuilder = new ProgressBuilder(LoginActivity.this);
        btnEnterStart = (Button) findViewById(R.id.btnEnterStart);
        btnResend = (TextView) findViewById(R.id.btnResend);
        tvCountryPhCode = (TextView) findViewById(R.id.tvCountryPhCode);
        linearLayoutOtp = (LinearLayout) findViewById(R.id.linearLayoutOtp);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etPhone.setTransformationMethod(new NumericKeyBoardTransformationMethod());

        etOtp1 = (EditText) findViewById(R.id.etOtp1);
        etOtp2 = (EditText) findViewById(R.id.etOtp2);
        etOtp3 = (EditText) findViewById(R.id.etOtp3);
        etOtp4 = (EditText) findViewById(R.id.etOtp4);

        etOtp1.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        etOtp2.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        etOtp3.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        etOtp4.setTransformationMethod(new NumericKeyBoardTransformationMethod());


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnEnterStart:
                if (btnEnterStart.getText().toString() == getString(R.string.enter)) {
                    getApiForOtp();
                } else if (btnEnterStart.getText().toString() == getString(R.string.start)) {
                    verifyOtp();
                }
                break;
            case R.id.btnResend:

                break;

        }
    }

    //call verify otp API
    private void verifyOtp() {

        if (!TextUtils.isEmpty(etPhone.getText().toString()) && Validation.hasOtpText(etOtp1, etOtp2, etOtp3, etOtp4)) {

            if(etPhone.toString().length()<10) {
                Toaster.showShortToast(LoginActivity.this, getString(R.string.enter_ten_digit_number));
                return;
            }
            if (!CommonFunctions.isOnline(LoginActivity.this)) {
                Toaster.showShortToast(LoginActivity.this, getString(R.string.internet_connection_not_available));
                return;
            } else {

                progressBuilder.showProgressDialog();
                ApiServiceInterface apiClient = AppClient.getClient().create(ApiServiceInterface.class);
                JsonObject object = new JsonObject();
                object.addProperty(ApiConstants.PHONE, tvCountryPhCode.getText().toString() + "-" + etPhone.getText().toString());
                object.addProperty(ApiConstants.OTP, etOtp1.getText().toString()+etOtp2.getText().toString()+etOtp3.getText().toString()+etOtp4.getText().toString());

                apiClient.postOtpVerify(object).enqueue(new Callback<OtpVerify>() {
                    @Override
                    public void onResponse(Call<OtpVerify> call, Response<OtpVerify> response) {
                        progressBuilder.dismissProgressDialog();
                        checkOtpVerifyResponseBody(response.body());
                    }

                    @Override
                    public void onFailure(Call<OtpVerify> call, Throwable t) {
                        progressBuilder.dismissProgressDialog();
                        Log.e("Throwable", "Throwable  : " + t.getMessage());

                    }
                });
            }
        } else {
            if (!Validation.hasOtpText(etOtp1, etOtp2, etOtp3, etOtp4) && TextUtils.isEmpty(etPhone.getText().toString()))
                Toaster.showShortToast(LoginActivity.this, getString(R.string.please_enter_phone_otp));
            else if (!Validation.hasOtpText(etOtp1, etOtp2, etOtp3, etOtp4))
                Toaster.showShortToast(LoginActivity.this, getString(R.string.please_enter_otp));
            else
                Toaster.showShortToast(LoginActivity.this, getString(R.string.please_enter_phone));
        }
    }

    private void checkOtpVerifyResponseBody(OtpVerify body) {

        switch (Integer.valueOf(body.getStatus())) {
            case 200:
                BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.PHONE_NUMBER, tvCountryPhCode.getText().toString() + "-" + etPhone.getText().toString()).commit();
                //Not registered UserStatus
                if (Integer.valueOf(body.getUserStatus()) == 0) {
                    startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                    finish();
                } else if (Integer.valueOf(body.getUserStatus()) == 1)// Registered UserStatus
                {
                    if (body.getUserDetails().size() > 0) {
                        ArrayList<UserDetails> userDetail = body.getUserDetails();
                        if(null!=userDetail.get(0)){

                            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.IS_USER_REGISTER, "Yes").commit();
                            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_FIRST_NAME, userDetail.get(0).first_name).commit();
                            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_LAST_NAME, userDetail.get(0).last_name).commit();
                            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.USER_EMAIL, userDetail.get(0).email).commit();
                        }

                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        if (null!= userDetail.get(0).bot_registered && userDetail.get(0).bot_registered.size() > 0)  // Bot Registered
                        {
                            Log.i(TAG, "Registered Bots count" + userDetail.get(0).bot_registered.size());
                            BotKitApp.editSharePrefs().putString(AppConstants.PREFERENCES.IS_BOT_REGISTER, "Yes").commit();

                            ArrayList<String> selectedBotIds = new ArrayList<String>();
                            for (int i = 0; i < userDetail.get(0).bot_registered.size(); i++) {
                                selectedBotIds.add(userDetail.get(0).bot_registered.get(i).id);
                            }
                            BotKitApp.saveBotList(selectedBotIds);
                        }
                        intent.putExtra("RegisteredBots", userDetail.get(0).bot_registered);
                        startActivity(intent);
                        finish();

                    }
//                    if(BotKitApp.sharedPreferences.getString(AppConstants.PREFERENCES.IS_BOT_REGISTER,"No").equalsIgnoreCase("No"))
//                        {
//
//                        }
//
//                    Toaster.showShortToast(LoginActivity.this, "user register call user info if user register bot call dashboard or call register bot ");
                }
                break;
            default:
                Toaster.showShortToast(LoginActivity.this, getString(R.string.serverbusy));

        }
    }

    //getOtp API call
    private void getApiForOtp() {

        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
            if (!CommonFunctions.isOnline(LoginActivity.this)) {
                Toaster.showShortToast(LoginActivity.this, getString(R.string.internet_connection_not_available));
                return;
            }
            progressBuilder.showProgressDialog();
            ApiServiceInterface apiClient = AppClient.getClient().create(ApiServiceInterface.class);

            JsonObject object = new JsonObject();
            object.addProperty(ApiConstants.PHONE, tvCountryPhCode.getText().toString() + "-" + etPhone.getText().toString());

            apiClient.getVerify(object).enqueue(new Callback<OtpVerify>() {
                @Override
                public void onResponse(Call<OtpVerify> call, Response<OtpVerify> response) {
                    progressBuilder.dismissProgressDialog();
                    if (response.isSuccessful())
                        checkResponseBody(response.body());
                }

                @Override
                public void onFailure(Call<OtpVerify> call, Throwable t) {
                    progressBuilder.dismissProgressDialog();
                    Toaster.showShortToast(LoginActivity.this, getString(R.string.serverFailure));
                }
            });

        } else {
            Toaster.showShortToast(LoginActivity.this, "Please enter phone number");
        }
    }

    private void checkResponseBody(OtpVerify body) {
        switch (Integer.valueOf(body.getStatus())) {
            case 200:
                // Make visible OTP UI & change button to START
                btnEnterStart.setText(getString(R.string.start));
                linearLayoutOtp.setVisibility(View.VISIBLE);
                btnResend.setVisibility(View.VISIBLE);
                break;
            default:
                Toaster.showShortToast(LoginActivity.this, getString(R.string.serverbusy));

        }


    }

    protected void showDialogCountry(String mCountry) {

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        Log.i("countryCode ", ": " + countryCode);
        String dialCode="";
        Log.i("countryCode1 ", ": " + Locale.getDefault().getCountry());


        // here is hard coded later need to change.

        //Log.i("Country ", ": " +picker.getAllCountries(LoginActivity.this));
        List<Country> ctr = picker.getAllCountries(LoginActivity.this);
        for (int i = 0; i < ctr.size(); i++) {
            if (ctr.get(i).getCode().equalsIgnoreCase(countryCode)) {
                dialCode=ctr.get(i).getDialCode();
                Log.i("getDialCode ", ": " + ctr.get(i).getDialCode());
                break;
            }
        }

        if(dialCode.trim().length()>0)
            tvCountryPhCode.setText(dialCode);
        else
            tvCountryPhCode.setText("+91");
        //Log.i("Country ", ": " + country);
    }

    //Country code picker
    public void conuntryList(View view) {
        picker.show(getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
    }

    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.etOtp1:
                    if (text.length() == 1)
                        etOtp2.requestFocus();

                    break;
                case R.id.etOtp2:
                    if (text.length() == 1)
                        etOtp3.requestFocus();
                    else if (text.length() == 0)
                        etOtp1.requestFocus();
                    break;
                case R.id.etOtp3:
                    if (text.length() == 1)
                        etOtp4.requestFocus();
                    else if (text.length() == 0)
                        etOtp2.requestFocus();
                    break;
                case R.id.etOtp4:
                    if (text.length() == 0)
                        etOtp3.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }
}
