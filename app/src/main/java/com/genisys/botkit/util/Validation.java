package com.genisys.botkit.util;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vishwanathd on 07-12-2017.
 */

public class Validation {
    // Regular Expression
    // change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //private static final String PHONE_REGEX = "\\d{3}-\\d{7}";
    public static final Pattern PHONE_PATTERN
            = Pattern.compile(                     // sdd = space, dot, or dash
            "(\\+[0-9]{5}+[\\- \\.]*)?"        // +<digits><sdd>*
                    + "(\\([0-9]{5}+\\)[\\- \\.]*)?"   // (<digits>)<sdd>*
                    + "([0-9][0-9\\- \\.]+[0-9]{5})"); // <digit><digit|sdd>+<digit>
    // Error MessageData
    private static final String EMAIL_MSG = "Invalid Email Address Format";
    //private static final String PHONE_MSG = "###-#######";

    // call this method when there is need to check email validation
    public static boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }

    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {
        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);
        // text required and editText is blank, so return false
        if (required && !hasText(editText)) return false;
        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            //editText.setError(errMsg);
            return false;
        }
        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        // length 0 means there is no text
        if (text.length() == 0) {
            return false;
        }
        return true;
    }
    public static boolean hasOtpText(EditText editText1,EditText editText2,EditText editText3,EditText editText4) {
        String text1 = editText1.getText().toString().trim();
        String text2 = editText2.getText().toString().trim();
        String text3 = editText3.getText().toString().trim();
        String text4 = editText4.getText().toString().trim();

        editText1.setError(null);
        editText2.setError(null);
        editText3.setError(null);
        editText4.setError(null);
        // length 0 means there is no text
        if (text1.length() == 0 || text2.length()==0 || text3.length()==0 || text4.length()==0) {
            return false;
        }
        return true;
    }


    public static boolean isFindPhoneNumberPattern(String content){
        Pattern phonePatter = PHONE_PATTERN;
        Matcher phoneMatcher=phonePatter.matcher(content);
        boolean isPhoneMachFound= phoneMatcher.find();
        return isPhoneMachFound;
    }
}
