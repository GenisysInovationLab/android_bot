package com.genisys.botkit.util;

import com.genisys.botkit.BuildConfig;

/**
 * Created by vishwanathd on 29-12-2017.
 */

public class ApiConstants {
    //user sign up keys
    public static final String FIRST_NAME ="first_name" ;
    public static final String LAST_NAME = "last_name";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String DOB = "dob";
    public static final String GENDER ="gender" ;
    public static final String OTP = "otp";
    public static String MEDIA="media";

    //chat keys
    public static final String TYPE="type";
    public static final String TEXT_CHAT="text";
    public static final String MESSAGE="message";
    public static final String USERID="userId";
    public static final String IMAGE_CHAT="image";
    public static final String HYBRID_CHAT = "hybrid";
    public static final String CALLBACK = "callback";
    public static final String BUTTON = "button";
    public static final String FILE_CHAT="file";
    public static final String BUTTON_CALLBACK_ACTION="callbackAction";
}
