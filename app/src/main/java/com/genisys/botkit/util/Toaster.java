package com.genisys.botkit.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by vishwanathd on 16-11-2017.
 */

public class Toaster {

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
