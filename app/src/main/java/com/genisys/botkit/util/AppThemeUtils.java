package com.genisys.botkit.util;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

/**
 * Created by vishwanathd on 02-05-2018.
 */

public class AppThemeUtils {
    public static GradientDrawable bgGradientDrawable(String startColor, String medColor, String endColor){
        String mColor=(null!=medColor)?medColor:startColor;
        String eColor=(null!=endColor)?endColor:startColor;
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {Color.parseColor(startColor),Color.parseColor(mColor),Color.parseColor(eColor)});
//        GradientDrawable gd =new GradientDrawable();
        gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
//        gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
//        gd.setShape(GradientDrawable.RECTANGLE);
//        gd.setColors( new int[] {Color.parseColor(startColor),Color.parseColor(medColor),Color.parseColor(endColor)});
        gd.setCornerRadius(0f);
        return gd;
    }
}
