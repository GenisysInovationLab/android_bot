package com.genisys.botkit.util;

/**
 * Created by vishwanathd on 01-12-2017.
 */

public class AppConstants
{
    public final static String HOST_NAME = "amqp://qqwyxzhs:ZiOcMSs0ZxRXJKCCBjVlofYqT3-eESW1@spider.rmq.cloudamqp.com/qqwyxzhs";
    public final static String CHANNEL_NAME ="travel-bot";
    public final static String CONSUMER_QUEUE_NAME="user_android";
    public final static String PUBLISHER_QUEUE_NAME = "bot_";
    public final static String MQ_MESSAGE_EXTRA="my_msg";
    public static final String REGISTRATION_FLOW="REGISTRATION_FLOW";
    public static final String EXTRA_CHAT_SCREEN_TITLE="EXTRA_CHAT_SCREEN_TITLE";
    public static final String EXTRA_CHAT_BOT_ID="EXTRA_CHAT_BOT_ID";

//    public static enum RanderType {TEXT_TYPE(0),IMAGE_TYPE(1);
//    private final int value;
//        RanderType (final int newvalue){
//            value=newvalue;
//        }
//        public int getValue() { return value; }
//    }
    /**
     * Do not change values of following constants as this might break app for existing users
     */
    public class PREFERENCES {
        public static final String USER_ID = "USER_ID";
        public static final String PHONE_NUMBER = "PHONE_NUMBER";
        public static final String IS_USER_REGISTER="IS_USER_REGISTER";
        public static final String USER_FIRST_NAME="USER_FIRST_NAME";
        public static final String USER_LAST_NAME="USER_LAST_NAME";
        public static final String USER_EMAIL="USER_EMAIL";
        public static final String IS_BOT_REGISTER="IS_BOT_REGISTER";
        public static final String SAVED_BOT_LIST="SAVED_BOT_LIST";

        public static final String USER_LOCAL_PHOTO_PATH = "USER_LOCAL_PHOTO_PATH";
        public static final String PROFILE_PHOTO_URL ="PROFILE_PHOTO_URL" ;
        public static final String PROFILE_PIC_FILE_ID ="PROFILE_PIC_FILE_ID" ;

        public static final String BOT_CHAT_THEME="BOT_CHAT_THEME";
       //public static final String APP_THEME_DEFAULT_VALUE="default";

    }
}
