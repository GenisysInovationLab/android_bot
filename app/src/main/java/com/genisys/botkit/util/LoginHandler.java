package com.genisys.botkit.util;

import android.app.Activity;
import android.content.Intent;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.activities.LoginActivity;

/**
 * Created by vishwanathd on 11-12-2017.
 */

public class LoginHandler {
    private static final String TAG = LoginHandler.class.getSimpleName();

    public static void logout(Activity activity){

        BotKitApp.editSharePrefs().remove(AppConstants.PREFERENCES.PHONE_NUMBER).commit();
        BotKitApp.editSharePrefs().remove(AppConstants.PREFERENCES.USER_FIRST_NAME).commit();
        BotKitApp.editSharePrefs().remove(AppConstants.PREFERENCES.USER_EMAIL).commit();
        BotKitApp.editSharePrefs().remove(AppConstants.PREFERENCES.SAVED_BOT_LIST).commit();
        BotKitApp.editSharePrefs().clear().commit();

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

}
