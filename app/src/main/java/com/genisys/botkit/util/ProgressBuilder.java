package com.genisys.botkit.util;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Window;

import com.genisys.botkit.R;

/**
 * Created by vishwanathd on 16-11-2017.
 */

public class ProgressBuilder {
    private Context context;
    private Dialog dialog;

    public ProgressBuilder(Context context) {
        this.context = context;
    }

    public Dialog showProgressDialog() {
        if (dialog == null) {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.dialog_progress_background));
            dialog.setContentView(R.layout.dialog_spinner);
            dialog.setCancelable(false);
        }
        dialog.show();
        return dialog;
    }

    public void dismissProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}

