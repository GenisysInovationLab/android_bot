package com.genisys.botkit.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genisys.botkit.R;
import com.genisys.botkit.api.pojo.Form;
import com.genisys.botkit.util.CommonFunctions;

/**
 * Created by vishwanathd on 15-01-2018.
 */

public class GuiEditBox extends LinearLayout {
    TextView label;
    EditText txtBox;

    public GuiEditBox(Context context, String labelText, String initialText) {
        super(context);
        label = new TextView(context);
        label.setText(labelText);
        label.setTextColor(Color.BLACK);
        LinearLayout.LayoutParams labelParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        labelParm.setMargins(10, 10, 0, 10);
        labelParm.weight = 0.3f;
        label.setLayoutParams(labelParm);

        txtBox = new EditText(context);
        txtBox.setText(initialText);
        txtBox.setTextColor(Color.BLACK);

        txtBox.setPadding(10, 10, 10, 10);
       // LinearLayout ll=new LinearLayout(context);
        LinearLayout.LayoutParams txtboxParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtboxParm.setMargins(20, 10, 10, 10);
        txtboxParm.weight = 0.7f;
        //ll.setLayoutParams(txtboxParm);
        txtBox.setLayoutParams(txtboxParm);


        this.addView(label);
        this.addView(txtBox);
    }
    public GuiEditBox(Context context, String labelText, String initialText,Form.Theme theme) {
        super(context);
        label = new TextView(context);
        label.setText(labelText);
        label.setTextSize(16);
        label.setTextColor(Color.parseColor(theme.title_text_font_color));
        LinearLayout.LayoutParams labelParm = new LayoutParams(0, LayoutParams.WRAP_CONTENT);
        labelParm.setMargins(10, 10, 0, 10);
        labelParm.weight = 0.3f;
        label.setPadding(CommonFunctions.convertDpToPx(10),0,0,0);
        label.setLayoutParams(labelParm);
        label.setGravity(Gravity.CENTER_VERTICAL);
        label.setMinHeight(CommonFunctions.convertDpToPx(40));
        label.setBackgroundColor(Color.parseColor(theme.title_text_bg_color));
        txtBox = new EditText(context);
        txtBox.setText(initialText);
        txtBox.setTextSize(16);
        txtBox.setHintTextColor(Color.GRAY);
        txtBox.setTextColor(Color.parseColor(theme.text_box_font_color));

        txtBox.setPadding(10, 10, 10, 10);



        LinearLayout.LayoutParams txtboxParm = new LayoutParams(0,LayoutParams.WRAP_CONTENT);
        txtboxParm.setMargins(20, 10, 10, 10);
        txtboxParm.weight = 0.7f;
        txtBox.setLayoutParams(txtboxParm);
        txtBox.setMinHeight(CommonFunctions.convertDpToPx(40));
        txtBox.setBackgroundColor(Color.parseColor(theme.text_box_bg_color));

        this.addView(label);
        this.addView(txtBox);
    }

    public GuiEditBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }


    public void makeNumeric() {
        DigitsKeyListener dkl = new DigitsKeyListener(true, true);
        txtBox.setKeyListener(dkl);
    }

    public void setEditTextBackground(int edit_text_bg) {


        txtBox.setBackgroundResource(R.drawable.rounded_edittext);
        GradientDrawable drawable = (GradientDrawable)txtBox.getBackground();
        drawable.setColor(Color.BLUE);
        
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            txtBox.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_text_bg));
        } else {
            txtBox.setBackground(getResources().getDrawable(R.drawable.edit_text_bg));
        }

    }

    public String getValue() {
        return txtBox.getText().toString();
    }

    public void setValue(String v) {
        txtBox.setText(v);
    }


}

