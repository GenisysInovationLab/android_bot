package com.genisys.botkit.widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genisys.botkit.api.pojo.Form;
import com.genisys.botkit.util.CommonFunctions;

/**
 * Created by vishwanathd on 17-01-2018.
 */

public class GuiDate extends LinearLayout {
    TextView label;
    TextView btn;


    public GuiDate(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public GuiDate(Context context, String labelText, String initialText, Form.Theme theme) {
        super(context);
        this.setClickable(true);
       // this.setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
        label = new TextView(context);
        label.setText(labelText);
        label.setTextColor(Color.parseColor(theme.title_text_font_color));
        LinearLayout.LayoutParams labelParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        labelParm.setMargins(10, 10, 0, 10);
        labelParm.weight = 0.3f;
        label.setPadding(CommonFunctions.convertDpToPx(10),0,0,0);
        label.setLayoutParams(labelParm);
        label.setGravity(Gravity.CENTER_VERTICAL);
        label.setClickable(false);
        label.setMinHeight(CommonFunctions.convertDpToPx(40));
        label.setBackgroundColor(Color.parseColor(theme.title_text_bg_color));

        btn=new TextView(context);
        btn.setTextColor(Color.parseColor(theme.text_box_font_color));
        btn.setText(initialText);
       // btn.setPadding(10, 10, 10, 10);

        LinearLayout.LayoutParams txtboxParm = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtboxParm.setMargins(10, 10, 10, 10);
        txtboxParm.weight = 0.7f;
        btn.setLayoutParams(txtboxParm);
        btn.setMinHeight(CommonFunctions.convertDpToPx(40));
        btn.setBackgroundColor(Color.parseColor(theme.text_box_bg_color));

        btn.setClickable(false);
        btn.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        this.addView(label);
        this.addView(btn);

    }
    public GuiDate(Context context, String labelText, String initialText) {
        super(context);
//        this.setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
//        label = new TextView(context);
//        label.setText(labelText);
//        label.setTextColor(Color.BLACK);
//        LinearLayout.LayoutParams labelParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
//        labelParm.setMargins(10, 10, 10, 10);
//        labelParm.weight = 0.3f;
//        label.setLayoutParams(labelParm);
//        btn=new Button(context);
//        btn.setBackgroundColor(Color.TRANSPARENT);
//
//        btn.setTextColor(Color.BLACK);
//        btn.setText(initialText);
//
//        btn.setPadding(10, 10, 10, 10);

 //       LinearLayout.LayoutParams txtboxParm = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
//        txtboxParm.setMargins(10, 10, 10, 10);
//        txtboxParm.weight = 0.7f;
//        btn.setLayoutParams(txtboxParm);
//
//        this.addView(label);
//        this.addView(btn);

    }
    public void setButtonBackground(int bg) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            this.setBackgroundDrawable(getResources().getDrawable(bg));
        } else {
            this.setBackground(getResources().getDrawable(bg));
        }

    }


    public void setValue(String s) {
        btn.setText(s);
    }
    public String getValue() {
        return btn.getText().toString();
    }
}
