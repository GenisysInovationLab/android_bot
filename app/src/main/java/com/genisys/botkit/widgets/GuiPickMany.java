package com.genisys.botkit.widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genisys.botkit.api.pojo.Form;
import com.genisys.botkit.util.CommonFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishwanathd on 11-05-2018.
 */

public class GuiPickMany extends LinearLayout implements CompoundButton.OnCheckedChangeListener {
    String tag=GuiPickMany.class.getName();
    CheckBox cb;
    Context ctx;
    TextView label;
    LinearLayout checkboxLayout;
    ArrayList<String> selectedOptions;
    public GuiPickMany(Context context, String labelText, List<String> options, Form.Theme theme) {
        super(context);
        this.ctx=context;
        selectedOptions=new ArrayList();
        this.setOrientation(VERTICAL);
        label = new TextView(context);
        label.setText(labelText);
        label.setTextSize(16);
        label.setTextColor(Color.parseColor(theme.title_text_font_color));
        LinearLayout.LayoutParams labelParm = new LayoutParams(LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        labelParm.setMargins(10, 10, 0, 10);
        //labelParm.weight = 0.3f;
        label.setLayoutParams(labelParm);
        label.setGravity(Gravity.CENTER_VERTICAL);
        label.setMinHeight(CommonFunctions.convertDpToPx(40));
        label.setBackgroundColor(Color.parseColor(theme.title_text_bg_color));
        label.setPadding(CommonFunctions.convertDpToPx(10), 0, 0, 0);

        this.addView(label);

        checkboxLayout=new LinearLayout(context);
        checkboxLayout.setOrientation(VERTICAL);

        for(int i=0;i<options.size();i++)
       {
           cb=new CheckBox(ctx);
           LinearLayout.LayoutParams labelParm2= new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
           labelParm2.setMargins(10, 10, 0, 10);
           cb.setText(options.get(i));
           cb.setTextColor(Color.parseColor(theme.title_text_font_color));
           cb.setOnCheckedChangeListener(this);
           this.checkboxLayout.addView(cb);

       }
        this.addView(this.checkboxLayout);

    }
    public ArrayList<String> getValue() {

        CheckBox b;
        if(null!=this.checkboxLayout && this.checkboxLayout.getChildCount()>0){
            for(int i=0;i<this.checkboxLayout.getChildCount();i++) {
                b=(CheckBox) this.checkboxLayout.getChildAt(i);
                if(b.isChecked());
                selectedOptions.add(b.getText().toString());
            }
        }
        return selectedOptions;
    }
    public GuiPickMany(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
            selectedOptions.add(buttonView.getText().toString());
        else if(!isChecked && !selectedOptions.isEmpty())
            selectedOptions.remove(buttonView.getText().toString());

        Toast.makeText(ctx,": "+selectedOptions,Toast.LENGTH_SHORT).show();

    }
}
