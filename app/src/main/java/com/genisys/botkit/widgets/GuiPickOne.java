package com.genisys.botkit.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.genisys.botkit.R;
import com.genisys.botkit.api.pojo.Form;
import com.genisys.botkit.util.CommonFunctions;

import java.util.List;

/**
 * Created by vishwanathd on 15-01-2018.
 */

public class GuiPickOne extends LinearLayout {
    String tag = GuiPickOne.class.getName();
    TextView label;
    ArrayAdapter<String> aa;
    CustomSpinnerAdapter customSpinnerAdapter;
    Spinner spinner;
Context ctx;
    public GuiPickOne(Context context, String labelText, List<String> options) {
        super(context);
        this.ctx=context;
        label = new TextView(context);
        label.setText(labelText);
        label.setTextColor(Color.BLACK);
        LinearLayout.LayoutParams labelParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        labelParm.setMargins(10, 10, 0, 10);
        labelParm.weight = 0.3f;
        label.setLayoutParams(labelParm);

        spinner = new Spinner(context);

        LinearLayout.LayoutParams spinnerParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParm.setMargins(0, 10, 10, 10);
//        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//            spinner.setBackgroundDrawable( getResources().getDrawable(R.drawable.inputbg) );
//        } else {
//            spinner.setBackground( getResources().getDrawable(R.drawable.inputbg));
//        }
        spinnerParm.weight = 0.7f;
        spinner.setLayoutParams(spinnerParm);


        //String []opts = options.split("\\|");
        aa = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, options);
        spinner.setAdapter(aa);
//        TextView oTextView = (TextView)spinner.getChildAt(0);
//        oTextView.setTextColor(Color.WHITE);
        this.addView(label);
        this.addView(spinner);
    }

    public GuiPickOne(Context context, String labelText, List<String> options, Form.Theme theme) {
        super(context);
        label = new TextView(context);
        label.setText(labelText);
        label.setTextSize(16);
        label.setTextColor(Color.parseColor(theme.title_text_font_color));
        LinearLayout.LayoutParams labelParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        labelParm.setMargins(10, 10, 0, 10);
        labelParm.weight = 0.3f;
        label.setLayoutParams(labelParm);
        label.setGravity(Gravity.CENTER_VERTICAL);
        label.setMinHeight(CommonFunctions.convertDpToPx(40));
        label.setBackgroundColor(Color.parseColor(theme.title_text_bg_color));
        label.setPadding(CommonFunctions.convertDpToPx(10), 0, 0, 0);

        spinner = new Spinner(context);

        LinearLayout.LayoutParams spinnerParm = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParm.setMargins(10, 10, 10, 10);
//
        spinnerParm.weight = 0.7f;
        spinner.setLayoutParams(spinnerParm);


        spinner.setGravity(Gravity.CENTER_VERTICAL);
        spinner.setMinimumHeight(CommonFunctions.convertDpToPx(40));
        spinner.setBackgroundColor(Color.parseColor(theme.text_box_bg_color));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTextColor(Color.parseColor(theme.text_box_font_color)); //Change selected text color

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //String []opts = options.split("\\|");
        customSpinnerAdapter=new CustomSpinnerAdapter(context,options,theme.text_box_font_color,theme.text_box_bg_color);
//        aa = new ArrayAdapter<String>(context, R.layout.dropdowntext, options) {
//
//
//            @Override
//            public View getDropDownView(int position, View convertView,
//                                        ViewGroup parent) {
//                View view = super.getDropDownView(position, convertView, parent);
//                TextView tv = (TextView) view;
//                // Set the item text color
//                tv.setTextColor(Color.parseColor(theme.text_box_font_color));
//                // Set the item background color
//                tv.setBackgroundColor(Color.parseColor(theme.text_box_bg_color));
//                return view;
//            }
//        };
//        aa.setDropDownViewResource(R.layout.dropdowntext);

        spinner.setAdapter(customSpinnerAdapter);

        this.addView(label);
        this.addView(spinner);

//        TextView oTextView = (TextView)spinner.getChildAt(0);
//        oTextView.setTextColor(Color.RED);
    }

    public GuiPickOne(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }


    public String getValue() {
        return (String) spinner.getSelectedItem().toString();
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private List<String> asr;
        String txtColor,txtBgColor;

        public CustomSpinnerAdapter(Context context,List<String> asr,String txt_ic_down_color,String txt_bg_color) {
            this.asr=asr;
            activity = context;
            txtColor=txt_ic_down_color;
            txtBgColor=txt_bg_color;
        }



        public int getCount()
        {
            return asr.size();
        }

        public Object getItem(int i)
        {
            return asr.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }



        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(activity);
            //txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(asr.get(position));
            txt.setTextColor(Color.parseColor(txtColor));
            txt.setBackgroundColor(Color.parseColor(txtBgColor));
            return  txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(activity);
            txt.setGravity(Gravity.CENTER);
           // txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            Drawable mDrawable2 = activity.getResources().getDrawable(R.drawable.arrow_down).mutate();
            mDrawable2.setColorFilter(new PorterDuffColorFilter(Color.parseColor(txtColor), PorterDuff.Mode.SRC_IN));

            //txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
           txt.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawable2, null);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor(txtColor));
            return  txt;
        }

    }
}
