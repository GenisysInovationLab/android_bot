package com.genisys.botkit;

public class TestData {

    public static final String formResp = "{\"message\":[{\"type\":\"form\",\"text\":\"ticket booking form\"}],\"userId\":\"user_android\",\"botId\":\"bot_1\",\"form\":{\"id\":\"form id\",\"name\":\"Form Name\",\"submitTo\":\"send url\",\"theme\":{\"bg_color\":\"#277C88\",\"font_size\":\"16\",\"title_text_font_color\":\"#ffffff\",\"title_text_bg_color\":\"#000000\",\"text_box_font_color\":\"#ffffff\",\"text_box_bg_color\":\"#000000\",\"button_color\":\"#000000\",\"button_text_color\":\"#ffffff\",\"bg_imgae_url\":\"bg image url\",\"logo_image_url\":\"logo image url\"},\"title\":{\"type\":\"label\",\"name\":\"titleLogin\",\"label\":\"Log In\",\"fontSize\":\"20\",\"alain\":\"left | center | left\"},\"field\":[{\"type\":\"textbox\",\"name\":\"fname\",\"label\":\"First Name\",\"required\":\"Y\",\"options\":[]},{\"type\":\"textbox\",\"name\":\"lname\",\"label\":\"Last Name\",\"required\":\"Y\",\"options\":[]},{\"type\":\"choice\",\"name\":\"gender\",\"label\":\"Gender\",\"required\":\"Y\",\"options\":[\"Male\",\"Female\"]},{\"type\":\"multichoice\",\"name\":\"interest\",\"label\":\"Interested\",\"required\":\"Y\",\"options\":[\"Sprots\",\"Dance\",\"Cooking\",\"Reading\"]},{\"type\":\"numeric\",\"name\":\"age\",\"label\":\"Age\",\"required\":\"N\",\"options\":[]},{\"type\":\"date\",\"name\":\"dob\",\"label\":\"Date Of Birth\",\"required\":\"N\",\"options\":[]}]}}";
    public static final String botListResp="{\"botlist\":[{\"_id\":\"5a8553f710d33c78575a4d36\",\"id\":\"1\",\"name\":\"Travel Bot\",\"description\":\"This bot will take care of your travel needs\",\"photo_url\":\"https://od.lk/s/119127432_OQkeb/tumblr_of4zhq2tW71r2qr2so1_500.jpg\",\"total_users\":\"10\",\"created_on\":\"04/12/2017\",\"owner\":\"Venkat\",\"isPublic\":\"1\"},{\"_id\":\"5a85542810d33c78575a4d37\",\"id\":\"2\",\"name\":\"Timesheet Bot\",\"description\":\"This bot helps Genisys employees to find their timesheet and book conference room for meetings.\",\"photo_url\":\"https://od.lk/s/119127428_v4Z7p/tumblr_oevnqoJ5XF1to7kzro1_500.jpg\",\"total_users\":\"8\",\"created_on\":\"04/12/2017\",\"owner\":\"Venkat\",\"isPublic\":\"0\"},{\"_id\":\"5a85545310d33c78575a4d38\",\"id\":\"3\",\"name\":\"PreSales Bot\",\"description\":\"This bot helps Genisys Sales team to prepare for client meetings.\",\"photo_url\":\"https://od.lk/s/119127427_zVuff/dfsa.jpg\",\"total_users\":\"8\",\"created_on\":\"04/12/2017\",\"owner\":\"Venkat\",\"isPublic\":\"0\"},{\"_id\":\"5b0689c7421f706c6c1757b6\",\"id\":\"4\",\"name\":\"Media Analytics\",\"description\":\"This bot is used to show media analytics\",\"photo_url\":\"https://od.lk/s/119278210_t6dzB/tumblr_of6xe7aeLR1rnng97o10%20copy.jpg\",\"total_users\":\"20\",\"created_on\":\"05/24/2018\",\"owner\":\"Naman Jain\",\"isPublic\":\"1\"},{\"_id\":\"5b0689f3421f706c6c1757b7\",\"id\":\"5\",\"name\":\"Text Analytics\",\"description\":\"This bot is used to find all analytics on text\",\"photo_url\":\"https://od.lk/s/119278211_JxyIb/tumblr_of6xe7aeLR1rnng97o10.jpg\",\"total_users\":\"20\",\"created_on\":\"05/24/2018\",\"owner\":\"Abhishek Dixit\",\"isPublic\":\"1\"}]}";

    public static final String fileData = "{" +
            "\"message\": [{" +
            "\"type\": \"file\"," +
            "\"filetype\": \"xls\"," +
            "\"text\": \"file title\"," +
            "\"filelink\": \"https://genisysgroup.sharepoint.com/:p:/r/sales/toolkit/_layouts/15/doc.aspx?sourcedoc=%7Bdd6dae0a-2a45-4835-ba67-be19ef2f7a94%7D&action=edit&uid=%7BDD6DAE0A-2A45-4835-BA67-BE19EF2F7A94%7D&ListItemId=69&ListId=%7BA4042F3A-9205-4CAA-930C-877807F6603A%7D&odsp=1&env=prod\"" +
            "}]," +
            "\"botId\": \"bot_1\"" +
            "}";
    public static final String buttonRes = "{" + "\"message\": [{" +
            "\"type\": \"button\"," +
            "\"text\": \"Genisys work with following domains \"," +
            "\"button\": [{" +
            "\"text\": \"button1\"," +
            "\"callbackAction\": \"callback1\"," +
            "\"clickMessage\": \"\"" +
            "}, {" +
            "\"text\": \"button2\"," +
            "\"callbackAction\": \"callback2\"," +
            "\"clickMessage\": \"\"" +
            "}, {" +
            "\"text\": \"button3\"," +
            "\"callbackAction\": \"callback3\"," +
            "\"clickMessage\": \"\"" +
            "}, {" +
            "\"text\": \"button4\"," +
            "\"callbackAction\": \"callback4\"," +
            "\"clickMessage\": \"\"" +
            "}, {" +
            "\"text\": \"button5\"," +
            "\"callbackAction\": \"callback5\"," +
            "\"clickMessage\": \"\"" +
            "}]" +
            "}]," +
            "\"userId\": \"5a2541075df1211777d02c88\"," +
            "\"botId\": \"bot_1\"" +
            "}";

}
