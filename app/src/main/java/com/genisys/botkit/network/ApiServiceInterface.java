package com.genisys.botkit.network;

import com.genisys.botkit.api.pojo.AllBots;
import com.genisys.botkit.api.pojo.OtpVerify;
import com.genisys.botkit.api.pojo.UserInfo;
import com.genisys.botkit.api.pojo.UserSignup;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by vishwanathd on 10-11-2017.
 */

public interface ApiServiceInterface {

    //Call<>
    @Headers("Content-Type: application/json")
    @POST("/user/verify")
    Call<OtpVerify>getVerify(@Body JsonObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("/user/verifyotp")
    Call<OtpVerify>postOtpVerify(@Body JsonObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("/user/createUser")
    Call<UserSignup>postRegisterUser(@Body JsonObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("/user/getallbot")
    Call<AllBots>getAllBots();

    @Headers("Content-Type: application/json")
    @POST("/user/registerbot")
    Call<OtpVerify>postRegisterBot(@Body JsonObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("/user/info")
    Call<UserInfo>postUserInfo(@Body JsonObject jsonObject);


}
