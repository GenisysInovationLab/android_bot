package com.genisys.botkit.network.volley;

import android.text.TextUtils;
import android.util.Log;

import com.genisys.botkit.BuildConfig;
import com.genisys.botkit.api.pojo.UserSignup;
import com.genisys.botkit.util.ApiConstants;

import java.io.File;
import java.util.HashMap;

public class DataManager {
    private static final String TAG = DataManager.class.getSimpleName();
    private static DataManager singleton;
    private static final String PHOTO_UPLOAD_URL = BuildConfig.SERVER_BASE_URL +"/file/upload";
    public static String GET_IMAGE_DOMAIN= BuildConfig.SERVER_BASE_URL+"/file/getFile";

    private DataManager() {
    }

    public static DataManager getInstance() {
        if (singleton == null) {
            singleton = new DataManager();
        }
        return singleton;
    }


    private static <T> void makeMultipartRequest(final String url, HashMap<Object, Object>  params, HashMap<Object, File> imageParams, Class<T> className, ResultListenerNG<T> resultListenerNG) {
        Log.d(TAG, "Url : " + url);
        Log.d(TAG, "Params : " + params);
        Log.d(TAG, "Image Params : " + imageParams);
        MultipartRequest request = new MultipartRequest(url, params, imageParams, className, resultListenerNG);
        Log.d(TAG, "Content type : " + request.getBodyContentType());
        RequestManagerApi.getInstance().addToRequestQueue(request);
    }




    public void postRegisterUser(HashMap<Object, Object> userDetail, String photo
            , final ResultListenerNG<UserSignup> resultListenerNG) {
//
        HashMap<Object, File> imageParams = new HashMap<>();
        if (!TextUtils.isEmpty(photo))
            imageParams.put(ApiConstants.MEDIA, new File(photo));

        makeMultipartRequest(PHOTO_UPLOAD_URL, userDetail, imageParams, UserSignup.class, resultListenerNG);
    }


    }