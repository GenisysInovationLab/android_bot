package com.genisys.botkit.network.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MultipartRequest<T> extends Request<T> {
    private static final String TAG = MultipartRequest.class.getSimpleName();
    private final Class<T> clazz;
    private MultipartEntity entity = new MultipartEntity();
    private final Gson gson = new Gson();

    private final Response.Listener<T> mListener;
    private final HashMap<Object, Object> mParams;
    private  JSONObject mStringParams;
    private final HashMap<Object, File> mImageParams;

    private MultipartRequest(String url, HashMap<Object, Object> params, HashMap<Object, File> imageParams, final Class<T> clazz,
                            Response.ErrorListener errorListener,
                            Response.Listener<T> listener) {
        super(Method.POST, url, errorListener);
        // Added to avoid com.android.volley.TimeoutError
        setShouldCache(false);
        setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 25, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mListener = listener;
        mParams = params;
        mImageParams = imageParams;
        this.clazz = clazz;
        buildMultipartEntity();
    }
    private MultipartRequest(String url, JSONObject params, HashMap<Object, File> imageParams, final Class<T> clazz,
                             Response.ErrorListener errorListener,
                             Response.Listener<T> listener) {
        super(Method.POST, url, errorListener);
        // Added to avoid com.android.volley.TimeoutError
        setShouldCache(false);
        setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 25, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mListener = listener;
        mStringParams = params;
        mImageParams = imageParams;
        mParams = null;
        this.clazz = clazz;
        buildMultipartJSonEntity();
    }

    private void buildMultipartJSonEntity() {
        if (mImageParams != null) {
            Iterator it = mImageParams.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                try {
                    String key = (String) pair.getKey();
                    File file = (File) pair.getValue();
                    ByteArrayBody byteArrayBody = new ByteArrayBody(readBytesFromFile(file), "image/jpg", file.getName());
                    entity.addPart(key, byteArrayBody);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                it.remove();
            }
        }

//        try {
//            entity.addPart(entry.getKey().toString(), new StringBody(entry.getValue().toString()));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
    }

    public MultipartRequest(String url, HashMap<Object, Object> params, HashMap<Object, File> imageParams, final Class<T> clazz,
                            final ResultListenerNG<T> resultListenerNG) {
        this(url, params, imageParams, clazz, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                resultListenerNG.onError(error);
            }
        }, new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                resultListenerNG.onSuccess(response);
            }
        });
    }
    public MultipartRequest(String url, JSONObject params, HashMap<Object, File> imageParams, final Class<T> clazz,
                            final ResultListenerNG<T> resultListenerNG) {
        this(url, params, imageParams, clazz, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                resultListenerNG.onError(error);
            }
        }, new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                resultListenerNG.onSuccess(response);
            }
        });
    }


    @Override
    public String getBodyContentType() {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            Log.d(TAG,"parseNetworkResponse : "+json);
            JSONObject jsonObject = new JSONObject(json);
            return Response.success(
                    gson.fromJson(jsonObject.toString(), clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    private void buildMultipartEntity() {
//        for (Map.Entry<Object, File> entry : this.mImageParams.entrySet()) {
//            if (entry.getValue() != null) {
//                try {
//                    ByteArrayBody byteArrayBody = new ByteArrayBody(readBytesFromFile(entry.getValue()), "image/jpg", entry.getValue().getName());
//                    entity.addPart(entry.getKey().toString(), byteArrayBody);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        if (mImageParams != null) {
            Iterator it = mImageParams.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                try {
                    String key = (String) pair.getKey();
                    File file = (File) pair.getValue();
                    ByteArrayBody byteArrayBody = new ByteArrayBody(readBytesFromFile(file), "image/jpg", file.getName());
                    entity.addPart(key, byteArrayBody);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                it.remove();
            }
        }

        Log.d(TAG, "Adding query map entries to multipart request.");
        for (Map.Entry<Object, Object> entry : this.mParams.entrySet()) {
            if (entry.getValue() != null) {
                try {
                    entity.addPart(entry.getKey().toString(), new StringBody(entry.getValue().toString()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static byte[] readBytesFromFile(File file) throws IOException {
        // Get the size of the file
        final long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            throw new IOException("Could not completely read file " + file.getName() + " as it is too long (" + length + " bytes, max supported "
                    + Integer.MAX_VALUE + ")");
        }

        final InputStream is = new FileInputStream(file);
        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            is.close();
            throw new IOException("Could not completely read file " + file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;
    }
}
