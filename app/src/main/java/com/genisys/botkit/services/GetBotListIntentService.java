package com.genisys.botkit.services;

import android.app.IntentService;
import android.content.Intent;

import com.genisys.botkit.BotKitApp;
import com.genisys.botkit.api.pojo.AllBots;
import com.genisys.botkit.api.pojo.BotDetail;
import com.genisys.botkit.util.AppConstants;
import com.google.gson.Gson;


public class GetBotListIntentService extends IntentService {

    public GetBotListIntentService() {
        super("GetBotListIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

       //        Gson gson = new Gson();
//        AllBots chatMsg = gson.fromJson(TestData.botListResp, AllBots.class);
//        checkResponseBody(chatMsg);

//
//        ApiServiceInterface client = AppClient.getClient().create(ApiServiceInterface.class);
//
//        client.getAllBots().enqueue(new Callback<AllBots>() {
//            @Override
//            public void onResponse(Call<AllBots> call, Response<AllBots> response) {
//                if (response.isSuccessful())
//                    checkResponseBody(response.body());
//            }
//            @Override
//            public void onFailure(Call<AllBots> call, Throwable t) {
//                Log.d("Throwable", "Throwable  : " + t.getMessage());
//            }
//        });

    }
    private void checkResponseBody(AllBots body) {

        if (null != body.botlist && body.botlist.size() > 0) {
            for (int i = 0; i < body.botlist.size(); i++) {
                BotDetail bot = new BotDetail();
                bot.id = body.botlist.get(i).id;
                if(null!=body.botlist.get(i).style){
                    BotKitApp.saveBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME+bot.id,body.botlist.get(i).style);
                }else if(null==body.botlist.get(i).style){
                    BotKitApp.removeBotTheme(AppConstants.PREFERENCES.BOT_CHAT_THEME+bot.id);

                }
            }
        }
    }

}
