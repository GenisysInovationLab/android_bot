package com.genisys.botkit.receiver;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.util.Log;

public class MyPhoneStateListener extends PhoneStateListener {
    public int signalStrengthValue;
    public  ConnectivitySignalListener connectivityReceiverListener;
public MyPhoneStateListener(ConnectivitySignalListener ctx){
    this.connectivityReceiverListener=ctx;
}
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);

        int signalStrengthValue;
        if (signalStrength.isGsm()) {
            if (signalStrength.getGsmSignalStrength() != 99) {
                signalStrengthValue = (signalStrength.getGsmSignalStrength() * 2 )- 113;
                Log.i("GsmSignalStrength ", "!=99 value "+signalStrengthValue);
            } else {
            //    Log.i("Signal level ", "Value " + signalStrength.getLevel());
                signalStrengthValue = signalStrength.getGsmSignalStrength();
                Log.i("GsmSignalStrength ", "value "+signalStrengthValue);
            }
        } else {
            signalStrengthValue = signalStrength.getCdmaDbm();
            Log.i("CdmaDbm ", "value "+signalStrengthValue);
            //Log.i("Cdma Signal level ", "Value " + signalStrength.getLevel());

        }


        if(connectivityReceiverListener!=null)
            connectivityReceiverListener.signalStrength(signalStrengthValue);
       // txtSignalStr.setText("Signal Strength : " + signalStrengthValue);
    }
    public interface ConnectivitySignalListener{
        void signalStrength(int strength);
    }
}
